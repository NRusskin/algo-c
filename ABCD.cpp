/*J. ABCDE-последовательность
ограничение по времени на тест1 секунда
ограничение по памяти на тест64 мегабайта
вводстандартный ввод
выводстандартный вывод
International Biology Manufacturer (IBM) обнаружили, что органический материал на Марсе имеет ДНК, состоящий из 5 символов(a,b,c,d,e), вместо четырех компонентов ДНК на Земле. 
Однако в строке не может встречаться ни одна из следующих пар в качестве подстроки: cd, ce, ed и ee.
IBM заинтересовались сколько правильных Марсианских строк ДНК длины n возможно составить?
Входные данные
Входные данные содержат несколько тестов. Каждый тест содержит одно число n на отдельной строке. Последняя строка входных файлов содержит ноль.
Количество тестов не превосходит 100.
Число n лежит в пределах от 1 до 250 включительно.
Выходные данные
Для каждого теста выведите на отдельной строке количество правильных строк по модулю 999999937.*/

#include <iostream>
#include <vector>

using std::vector;

class Matrix {
public:
    Matrix(size_t t): sz(t), data(t) {
        for (size_t i = 0; i < t; ++i) {
            data[i].resize(t, 0);
            data[i][i] = 1;
        }
    }
    size_t size() const {
        return sz;
    }
    vector<long long>& operator[](long long i) {
        return data[i];
    }
    const vector<long long>&  operator[](long long i) const {
        return data[i];
    }
private:
    size_t sz;
    vector<vector<long long>> data;
};

Matrix MatrixMultiplication(const Matrix& a, const Matrix& b, long long mod) {
    Matrix tmp(a.size());
    for (size_t i = 0; i < a.size(); ++i) {
        for (size_t j = 0; j < a.size(); ++j) {
            long long p = 0;
            for (size_t k = 0; k < a.size(); ++k) {
                p += (a[i][k] * b[k][j]) % mod;
                p %= mod;
            }
            tmp[i][j] = p;
        }
    }
    return tmp;
}

Matrix binpow(Matrix a, long long n, long long mod) {
    Matrix res(a.size());
    while (n) {
        if (n & 1) {
            res = MatrixMultiplication(res, a, mod);
        }
        a = MatrixMultiplication(a, a, mod);
        n >>= 1;
    }
    return res;
}

int main() {
    const long long mod = 999999937;
    long long x;
    while (std::cin >> x) {
        if (x == 0) {
            return 0;
        }
        Matrix A(2);
        A[0] = {1,2};
        A[1] = {2,3};
        A = binpow(A, x + 1, mod);
        std::cout << A[0][0] << std::endl;
    }
    return 0;
}