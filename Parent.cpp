/*B. Предок
ограничение по времени на тест1 секунда
ограничение по памяти на тест256 мегабайт
вводстандартный ввод
выводстандартный вывод
Напишите программу, которая для двух вершин дерева определяет, является ли одна из них предком другой.
Входные данные
Первая строка входного файла содержит натуральное число n (1≤n≤100000) — количество вершин в дереве.
Во второй строке находится n чисел. При этом i-е число второй строки определяет непосредственного родителя вершины с номером i. 
Если номер родителя равен нулю, то вершина является корнем дерева.
В третьей строке находится число m (1≤m≤100000)  — количество запросов. 
Каждая из следующих m строк содержит два различных числа a и b (1≤a,b≤n).
Выходные данные
Для каждого из m запросов выведите на отдельной строке число 1, если вершина a является одним из предков вершины b, и 0 в противном случае.*/

#include <iostream>
#include <vector>

using std::vector;
using std::cin;
using std::cout;

class CGraph {
public:
    explicit CGraph(int count): adjacencyList(count) {}
    void AddEdge(int from, int to);
    int VerticesCount() const;
    const vector<int>& GetNextVertices(int vertex) const;
private:
    vector<vector<int>> adjacencyList;
};

void CGraph::AddEdge(int from, int to) {
    adjacencyList[from].push_back(to);
    adjacencyList[to].push_back(from);
}

int CGraph::VerticesCount() const {
    return adjacencyList.size();
}

const vector<int>& CGraph::GetNextVertices(int vertex) const {
    return adjacencyList[vertex];
}

enum Status {
    VISITED,
    ENTERED,
    NOT_VISITED
};

void dfs(int v, vector <int>& tin, vector<int>& tout, int& timer, vector<Status>& status,
    const CGraph& graph) {
    tin[v] = timer++;
    status[v] = ENTERED;
    for (auto i: graph.GetNextVertices(v)) {
        if (status[i] == NOT_VISITED) {
            dfs(i, tin, tout, timer, status, graph);
        }
    }
    status[v] = VISITED;
    tout[v] = timer++;
}

class BypassedGraph {
public:
    BypassedGraph(const CGraph& graph, int root);
    bool CheckSharedParent(int x, int y);
private:
    const CGraph& graph;
    vector<int> tin;
    vector<int> tout;
};

BypassedGraph::BypassedGraph(const CGraph& graph, int root): graph(graph), tin(graph.VerticesCount()), tout(graph.VerticesCount()) {
    int n = graph.VerticesCount();
    vector<int> neib;
    vector<Status> status(n, NOT_VISITED);
    int timer = 0;
    dfs(root, tin, tout, timer, status, graph);
}

bool BypassedGraph::CheckSharedParent(int x, int y) {
   return tin[x - 1] < tin[y - 1] && tout[y - 1] < tout[x - 1];
}

int main() {
    int n, root, x;
    cin >> n;
    CGraph graph(n);
    for (int i = 0; i < n; ++i) {
        cin >> x;
        if (x == 0) {
            root = i;
        }
        else {
            graph.AddEdge(i, x - 1);
        }
    }
    BypassedGraph BGraph(graph, root);
    int m, y;
    cin >> m;
    for (int i = 0; i < m; ++i) {
        cin >> x >> y;
        cout << BGraph.CheckSharedParent(x, y) << std::endl;
    }
    return 0;
}