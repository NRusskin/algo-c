/*D. Кратчайший путь
ограничение по времени на тест2 секунды
ограничение по памяти на тест64 мегабайта
вводстандартный ввод
выводстандартный вывод
Дан взвешенный ориентированный граф и вершина s в нем. Требуется для каждой вершины u найти длину кратчайшего пути из s в u.
Входные данные
Первая строка входного файла содержит n, m и s — количество вершин, ребер и номер выделенной вершины соответственно (2≤n≤2000, 1≤m≤6000).
Следующие m строк содержат описание ребер. Каждое ребро задается стартовой вершиной, конечной вершиной и весом ребра. 
Вес каждого ребра — целое число, не превосходящее 1015 по модулю. В графе могут быть кратные ребра и петли.
Выходные данные
Выведите n строк — для каждой вершины u выведите длину кратчайшего пути из s в u, '*' если не существует путь из s в u и '-' если 
не существует кратчайший путь из s в u.*/

#include <iostream>
#include <vector>
#include <limits>
#include <queue>

using std::vector;
using std::cin;
using std::cout;
using std::numeric_limits;
using std::queue;

struct CEdge {
    long long Weight;
    int Vertex;
    CEdge(int to, long long weight): Weight(weight), Vertex(to) {}
};

class CGraph {
public:
    explicit CGraph(int count): adjacencyList(count) {}
    void AddEdge(int from, int to, long long weight);
    int VerticesCount() const;
    const vector<CEdge>& GetNextVertices(int vertex) const;
private:
    vector<vector<CEdge>> adjacencyList;
};

void CGraph::AddEdge(int from, int to, long long weight) {
    adjacencyList[from].push_back(CEdge(to, weight));
}

int CGraph::VerticesCount() const {
    return adjacencyList.size();
}

const vector<CEdge>& CGraph::GetNextVertices(int vertex) const {
    return adjacencyList[vertex];
}

void shortestPathes(const CGraph& graph, int start, vector<long long>& dist, vector<bool>& used) {
    int n = graph.VerticesCount();
    dist[start] = 0;
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j) {
            for (auto vertex: graph.GetNextVertices(j)) {
                if (dist[j] < numeric_limits<long long>::max() && dist[vertex.Vertex] > dist[j] + vertex.Weight) {
                    dist[vertex.Vertex] = dist[j] + vertex.Weight;
                }
            }
        }
    }
    for (int i = 0; i < n; ++i) {
        for (auto vertex: graph.GetNextVertices(i)) {
            if (dist[i] < numeric_limits<long long>::max() && dist[vertex.Vertex] > dist[i] + vertex.Weight &&
                !used[vertex.Vertex]) {
                queue<int> que;
                que.push(vertex.Vertex);
                while (!que.empty()) {
                    int tmp = que.front();
                    que.pop();
                    used[tmp] = true;
                    for (auto u: graph.GetNextVertices(tmp)) {
                        if (!used[u.Vertex]) {
                            used[u.Vertex] = true;
                            que.push(u.Vertex);
                        }
                    }
                }
            }
        }
    }
}

int main() {
    int n, m, start;
    cin >> n >> m >> start;
    --start;
    CGraph graph(n);
    for (int i = 0; i < m; ++i) {
        int from, to;
        long long weight;
        cin >> from >> to >> weight;
        --from, --to;
        graph.AddEdge(from, to, weight);
    }
    vector<long long> dist(n, numeric_limits<long long>::max());
    vector<bool> used(n, false);
    shortestPathes(graph, start, dist, used);
    for (int i = 0; i < n; ++i) {
        if (dist[i] == numeric_limits<long long>::max()) {
            cout << "*";
        } 
        else if (used[i] == true) {
            cout << "-";
        }
        else {
            cout << dist[i];
        }
        cout << std::endl;
    }
}