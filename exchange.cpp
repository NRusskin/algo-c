/*B. Обмен
ограничение по времени на тест2 секунды
ограничение по памяти на тест256 мегабайт
вводстандартный ввод
выводстандартный вывод
Пусть все натуральные числа исходно организованы в список в естественном порядке. Разрешается выполнить следующую операцию: swap(a,b). Эта операция возвращает в качестве результата расстояние в текущем списке между числами a и b и меняет их местами.
Задана последовательность операций swap. Требуется вывести в выходной файл результат всех этих операций.
Входные данные
Первая строка входного файла содержит число n (1⩽n⩽200000) — количество операций. Каждая из следующих n строк содержит по два числа в диапазоне от 1 до 109 — аргументы операций swap.
Выходные данные
Для каждой операции во входном файле выведите ее результат.*/

#include <iostream>
#include <string>
#include <vector>
#include <list>
 
using std::vector;
using std::list;
using std::string;
 
const int p = 997;
const int mod = 558343;
 
struct Pair {
    const string key;
    int value;
    Pair (const string& k, int v): key(k), value(v) {}
};
        
class HashTable {
public:
    HashTable() {
        data.resize(mod);
    }
    void del (const string& k);
    void put (const string& k, int v);
    int get (const string& k);
private:
     vector<list<Pair>> data;
     int count_hash (const string& s);
     void delHash (int hash, const string& k);
};
 
void HashTable::delHash(int hash, const string& k) {
    for (auto it = data[hash].begin(); it != data[hash].end(); ) {
        if (it->key == k) {
            it = data[hash].erase(it);
            return;
        } 
        else ++it;
    }
    return;
}
 
void HashTable::del(const string& k) {
    int hash = count_hash(k);
    delHash(hash, k);
}
 
void HashTable::put (const string& k, int v) {
    int hash = count_hash(k);
    delHash(hash, k);
    data[hash].push_back(Pair(k,v));
    return;
}
 
int HashTable::get (const string& k) {
    int hash = count_hash(k);
    for (auto p:data[hash]) {
        if (p.key == k) {
            return p.value;
        }
    }
    return 0;
}
 
int HashTable::count_hash (const string& k) {
    int hash = 0; 
    int cur = 1;
    for (size_t i = 0; i < k.size();i++) {
        hash += int(k[i])*cur; 
        cur *= p; 
        cur %= mod; 
        hash %= mod;
    }
    return hash;
}
 
int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);
    std::cout.tie(nullptr);
    string s, x;
    int a,b,n;
    HashTable table;
    std::cin>>n;
    for (int i=0; i<n; ++i) {
        std::cin>>s>>x;
        if (table.get(s) == 0) {
            table.put(s,std::stoi(s));
        }
        if (table.get(x) == 0) {
            table.put(x,std::stoi(x));
        }
        a = table.get(s);
        b = table.get(x);
        table.put(s,b); 
        table.put(x,a);
        std::cout<<abs(a-b)<<std::endl;
    }
    return 0;
}