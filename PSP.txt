#include <iostream>
#include <stack>
#include <string>
bool check (std::string seq) {
    std::stack <int> st;
    for (int i=0; i<seq.size(); ++i) {
        if (seq[i] == '{' || seq[i] == '(' || seq[i] == '[') st.push(seq[i]);
        else 
        if (st.empty()) return false;
        else
        if (seq[i] == '}' && st.top() == '{' || seq[i] == ']' && st.top() == '['  || seq[i] == ')' && st.top() == '(' ) st.pop();
        else return false;
    }
    if (st.empty()) return true;
    else return false;
}
int main() {
    std::string seq;
    std::cin >> seq;
    if (check(seq)) std::cout << "yes";\
    else std::cout << "no";
    return 0;
}