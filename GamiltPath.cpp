/*F. Продавец аквариумов
ограничение по времени на тест1 секунда
ограничение по памяти на тест64 мегабайта
вводстандартный ввод
выводстандартный вывод
Продавец аквариумов для кошек хочет объехать n городов, посетив каждый из них ровно один раз. Помогите ему найти кратчайший путь.

Входные данные
Первая строка входного файла содержит натуральное число n (1⩽n⩽13) — количество городов. Следующие n строк содержат по n чисел — длины путей между городами.

В i-й строке j-е число — ai,j — это расстояние между городами i и j (0⩽ai,j⩽106; ai,j=aj,i; ai,i=0).

Выходные данные
В первой строке выходного файла выведите длину кратчайшего пути. Во второй строке выведите n чисел — порядок, в котором нужно посетить города.*/

#include <bits/stdc++.h>

using std::vector;

int shortestRouteWithPath(const vector < vector < int >> & data, vector < int > & answer) {
    int n = data.size();
    int maxmask = pow(2, n);
    vector < vector < int >> dp(n, vector < int > (maxmask, INT_MAX / 2));
    vector < vector < int >> p(n, vector < int > (maxmask, -1));
    for (int i = 0; i < n; ++i) {
        dp[i][pow(2, i)] = 0;
    }
    for (int mask = 1; mask < maxmask; ++mask) { //маска -- посещаем наиболее выгодно все города и заканчиваем в iтом городе + пересчет
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n; ++j) {
                if ((mask >> j) & 1) continue;
                int mask1 = mask | (1 << j);
                if (dp[i][mask] + data[i][j] < dp[j][mask1]) {
                    dp[j][mask1] = dp[i][mask] + data[i][j];
                    p[j][mask1] = i;
                }
            }
        }
    }
    int ans = INT_MAX;
    int m = n;
    for (int i = 0; i < n; ++i) {
        if (dp[i][maxmask - 1] < ans) {
            ans = dp[i][maxmask - 1];
            m = i;
        }
    }
    int mask = maxmask - 1;
    answer.clear();
    for (int i = 0; i < n - 1; ++i) {
        answer.push_back(m + 1);
        int t = m;
        m = p[m][mask];
        mask -= pow(2, t);
    }
    answer.push_back(m + 1);
    return ans;
}

int main() {
    int n;
    std::cin >> n;
    vector < vector < int >> data(n, vector < int > (n));
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j) {
            std::cin >> data[i][j];
        }
    }
    vector < int > answer;
    std::cout << shortestRouteWithPath(data, answer) << std::endl;
    for (int i = 0; i < n; ++i) {
        std::cout << answer[i] << " ";
    }
    return 0;
}