#include <iostream>
#include <vector>
 
struct Item {
    int weight;
    int cost;
    Item(int w, int c): weight(w), cost(c) {}
};
 
int calcMaximalCost (const std::vector<Item>& items, int w) {
    int n = items.size();
    std::vector<std::vector<int>> dp (n+1, std::vector<int>(w+1));
    for (int i = 0; i < w+1; ++i) {
        dp[0][i] = 0;
    }
    for (int i = 1; i < n+1; ++i) {
        for (int j = 0; j < w+1; ++j) {
            if (j < items[i-1].weight) {
                dp[i][j] = dp[i-1][j];
            }
            else {
                dp[i][j] = std::max(dp[i-1][j], dp[i-1][j-items[i-1].weight] + items[i-1].cost); 
            }
        }
    }  
    return dp[n][w];
}
 
int main() {
    int w, n, k;
    std::cin >> w >> n;
    std::vector<Item> items;
    items.reserve(n);
    for (int i = 0; i < n; ++i) {
        std::cin >> k;
        items.push_back(Item(k,k));
    }
    std::cout << calcMaximalCost(items, w);
    return 0;
}