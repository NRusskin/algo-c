/*C. K-ый максимум
ограничение по времени на тест 0.5 секунд
ограничение по памяти на тест 64 мегабайта
ввод стандартный ввод
вывод стандартный вывод
Напишите программу, реализующую структуру данных, позволяющую добавлять и удалять элементы, а также находить k-й максимум.
Входные данные
Первая строка входного файла содержит натуральное число n — количество команд (n≤100000). Последующие n строк содержат по одной команде каждая. Команда записывается в виде двух чисел ci и ki — тип и аргумент команды соответственно (|ki|≤109).
Поддерживаемые команды:
+1 (или просто 1): Добавить элемент с ключом ki.
0: Найти и вывести ki-й максимум.
−1: Удалить элемент с ключом ki.
Гарантируется, что в процессе работы в структуре не требуется хранить элементы с равными ключами или удалять несуществующие элементы. Также гарантируется, что при запросе ki-го максимума, он существует.
Выходные данные
Для каждой команды нулевого типа в выходной файл должна быть выведена строка, содержащая единственное число — ki-й максимум*/

#include <iostream>
#include <string>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <map>

using std::map;
using std::vector;
using std::pair;
using std::cout;
using std::cin;
using std::string;
//корневая декомпозиция с офлайн запросами и сжатием координат

struct KthMaximum {
public:
    KthMaximum(int q, const int block): block(block), querys(q), number_elements_in_block(block), decomposition(block, vector < int > (block)) {}
    void read(int q);
    void compress();
    void processing(int q);
private: 
    map <int, pair <int,int>> find;
    vector <pair <int, int>> querys;
    vector <int> to_compress;
    const int block;
    vector <int> number_elements_in_block;
    vector <vector <int>> decomposition;
    void add(int i);
    void erase(int i);
    void search_and_print(int k);
};

void KthMaximum::add(int i) {
    pair <int, int> current_query = find[querys[i].second];
    ++number_elements_in_block[current_query.first];
    ++decomposition[current_query.first][current_query.second];
}

void KthMaximum::erase(int i) {
    pair <int, int> current_query = find[querys[i].second];
    --number_elements_in_block[current_query.first];
    --decomposition[current_query.first][current_query.second];
}

void KthMaximum::search_and_print(int k) {
    int cursor = 0;
    int ans_block;
    for (int j = 0; j < block; ++j) {
        if (cursor + number_elements_in_block[j] >= k) {
            ans_block = j;
            break;
        }
        cursor += number_elements_in_block[j];
    }
    for (int j = 0; j < block; ++j) {
        if (cursor + decomposition[ans_block][j] == k) {
            cout << to_compress[ans_block * block + j] << "\n";
            break;
        }
        cursor += decomposition[ans_block][j];
    }
}

void KthMaximum::processing(int q) {
    int size = 0;
    for (int i = 0; i < q; ++i) {
        if (querys[i].first == 1) {
            add(i);
            ++size;
        }
        if (querys[i].first == 2) {
            erase(i);
            --size;
        }
        if (querys[i].first == 3) {
            int k = querys[i].second;
            k = size + 1 - k;
            search_and_print(k);
        }
    }
}

void KthMaximum::compress() {
    sort(to_compress.begin(), to_compress.end());
    for (size_t i = 0; i < to_compress.size(); i++) {
        find[to_compress[i]] = {
            i / block,
            i % block
        };
    }
}

void KthMaximum::read(int q) {
    for (int i = 0; i < q; ++i) {
        string s;
        int x;
        cin >> s >> x;
        if (s == "+1" || s == "1") {
            querys[i] = {1,x};
            to_compress.push_back(x);
        }
        if (s == "0") querys[i] = {3,x};
        if (s == "-1") querys[i] = {2,x};
    }
}

int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(0);
    std::cout.tie(0);
    int q;
    const int block = 1000;
    cin >> q;
    KthMaximum kth_max = KthMaximum(q, block);
    kth_max.read(q);
    kth_max.compress();
    kth_max.processing(q);
    return 0;
}