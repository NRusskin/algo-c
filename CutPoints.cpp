/*I. Точки сочленения
ограничение по времени на тест1 секунда
ограничение по памяти на тест256 мегабайт
вводстандартный ввод
выводстандартный вывод
Дан неориентированный граф без петель и кратных рёбер. Требуется найти все точки сочленения в нем.
Входные данные
Первая строка входного файла содержит два натуральных числа n и m — количество вершин и ребер графа соответственно (n≤20000,m≤200000).
Следующие m строк содержат описание ребер по одному на строке. 
Ребро номер i описывается двумя натуральными числами bi,ei — номерами концов ребра (1≤bi,ei≤n).
Выходные данные
Первая строка выходного файла должна содержать одно натуральное число k — количество точек сочленения в заданном графе. 
На следующей строке выведите k целых чисел — номера вершин, которые являются точками сочленения, в возрастающем порядке.*/

#include <iostream>
#include <vector>
#include <algorithm>

using std::vector;
using std::cin;
using std::cout;

class CGraph {
public:
    explicit CGraph(int count): adjacencyList(count) {}
    void AddEdge(int from, int to);
    int VerticesCount() const;
    const vector<int>& GetNextVertices(int vertex) const;
private:
    vector<vector<int>> adjacencyList;
};
 
void CGraph::AddEdge(int from, int to) {
    adjacencyList[from].push_back(to);
    adjacencyList[to].push_back(from);
}
 
int CGraph::VerticesCount() const {
    return adjacencyList.size();
}
 
const vector<int>& CGraph::GetNextVertices(int vertex) const {
    return adjacencyList[vertex];
}

void findCutPoints(int v, const CGraph& graph, vector<bool>& used, vector<int>& points, 
int& timer, vector<int>& tin, vector<int>& ret, int parent = -1) {
    used[v] = true;
    tin[v] = ret[v] = timer++;
    int count = 0;
    for (auto t: graph.GetNextVertices(v)) {
        if (t == parent) continue;
        if (used[t]) {
            ret[v] = std::min(ret[v], tin[t]);
        }
        else {
            findCutPoints(t, graph, used, points, timer, tin, ret, v);
            ret[v] = std::min(ret[v], ret[t]);
            if (ret[t] >= tin[v] && parent != -1) {
                points.push_back(v + 1);
            }
            ++count;
        }
    }
    if (parent == -1 && count > 1) { // т. к. у корня дерева обхода свое условие на точку сочленения 
        points.push_back(v + 1);
    }
}

void findCutPoints(const CGraph& graph, vector<int>& points) {
    int n = graph.VerticesCount();
    vector<bool> used(n, false);
    int timer = 0;
    vector<int> tin(n), ret(n);
    for (int i = 0; i < n; ++i){
        if (!used[i]) {
            findCutPoints(i, graph, used, points, timer, tin, ret);
        }
    }
    std::sort(points.begin(), points.end());
    points.resize(unique(points.begin(),points.end())-points.begin());
}

int main() {
    int n, m, x, y;
    cin >> n >> m;
    CGraph graph(n);
    for (int i = 0; i < m; ++i) {
        cin >> x >> y;
        --x;
        --y;
        graph.AddEdge(x, y);
    }
    vector<int> points;
    findCutPoints(graph, points);
    cout << points.size() << std::endl;
    for (auto point: points) {
        cout << point << ' ';
    }
    return 0;
}