/*H. Вторая статистика (RMQ)
ограничение по времени на тест1 секунда
ограничение по памяти на тест64 мегабайта
вводстандартный ввод
выводстандартный вывод
Дано число N и последовательность из N целых чисел. Найти вторую порядковую статистику на заданных диапазонах.

Для решения задачи используйте структуру данных Sparse Table. Требуемое время обработки каждого диапазона O(1). Время подготовки структуры данных O(nlogn).

Входные данные
В первой строке заданы 2 числа: размер последовательности N и количество диапазонов M.

Следующие N целых чисел задают последовательность. Далее вводятся M пар чисел - границ диапазонов.

Выходные данные
Для каждого из M диапазонов напечатать элемент последовательности - 2ю порядковую статистику. По одному числу в строке.*/

#include <iostream>
#include <vector>
#include <cmath>
#include <limits.h>

class SparseTable {
public:
    SparseTable(int n, const std::vector <int>& p);
    std::pair <int,int> getMin(std::pair <int,int> l, std::pair <int,int> r);
    void buildTable(int n);
    int getSecondKStat(int l, int r);
private:
    std::vector <std::vector<std::pair<int,int>>> data;
    int ln2;
};

SparseTable::SparseTable(int n, const std::vector <int>& p) {
    ln2 = ceil(log(n)/log(2));
    data.assign(ln2, std::vector < std::pair < int, int >> (n, {0,0}));
    for (int i = 0; i < n; ++i)
        data[0][i] = {p[i],INT_MAX};
}

std::pair <int,int> SparseTable::getMin(std::pair <int,int> l, std::pair <int,int> r) {
    std::pair < int, int > minPair;
    if (l.first <= r.first) {
        minPair.first = l.first;
        if ((r.first <= l.second || l.second == l.first) && r.first != l.first)
            minPair.second = r.first;
        else if (l.second <= r.second && l.second != l.first)
            minPair.second = l.second;
        else minPair.second = r.second;
    } 
    else {
        minPair.first = r.first;
        if ((l.first <= r.second || r.second == r.first) && l.first != r.first)
            minPair.second = l.first;
        else if (r.second <= l.second && r.second != r.first)
            minPair.second = r.second;
        else minPair.second = l.second;
    }
    return minPair;
}

void SparseTable::buildTable(int n) {
    int len = n;
    for (int i = 1; i <= ln2; ++i) {
        int nlen = 0;
        int diff = (1 << (i - 1));
        for (int j = 0; j + diff < len; ++j, ++nlen) {
            std::pair < int, int > minPair(INT_MAX, INT_MAX);
            minPair = getMin(data[i - 1][j], data[i - 1][j + diff]);
            data[i][j] = minPair;
        }
        len = nlen;
    }
}

int SparseTable::getSecondKStat(int l, int r) {
    int k;
    for (k = 1; (r - l) + 1 > (1 << k) - 1; ++k) {}
    --k;
    r = r - (1 << k) + 1;
    std::pair < int, int > minPair(INT_MAX, INT_MAX);
    if (r > l)
        minPair = getMin(data[k][l], data[k][r]);
    else minPair = getMin(data[k][l], minPair);
    return minPair.second;
}

int main() {
    int n, m, l, r;
    std::cin >> n >> m;
    std::vector <int> p(n);
    for (int i = 0; i < n; ++i)
        std::cin >> p[i];
    SparseTable table(n, p);
    table.buildTable(n);
    for (int i = 0; i < m; ++i) {
        std::cin >> l >> r;
        std::cout << table.getSecondKStat(l - 1, r - 1) << std::endl;
    }
    return 0;
}