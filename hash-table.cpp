/*E. Хеш-таблица
ограничение по времени на тест2 секунды
ограничение по памяти на тест512 мегабайт
вводmap.in
выводmap.out
Реализуйте ассоциативный массив с использованием хеш-таблицы. Использовать стандартную библиотеку (set, map, LinkedHashMap, и т. п.) не разрешается.
Входные данные
Входной файл содержит описание операций, их количество не превышает 100000. В каждой строке находится одна из следующих операций:
put x y — поставить в соответствие ключу x значение y. Если ключ уже есть, то значение необходимо изменить.
delete x — удалить ключ x. Если элемента x нет, то ничего делать не надо.
get x — если ключ x есть в ассоциативном массиве, то выведите соответствующее ему значение, иначе выведите «none».
Ключи и значения — строки из латинских букв длинной не более 20 символов.
Выходные данные
Выведите последовательно результат выполнения всех операций get. Следуйте формату выходного файла из примера.*/

#include <iostream>
#include <string>
#include <vector>
#include <list>

using std::vector;
using std::list;
using std::string;

const int p = 997;
const int mod = 99998;

struct Pair {
    const string key;
    const string value;
    Pair (const string& k, const string& v): key(k), value(v) {}
};

class HashTable {
public:
    HashTable() {
        data.resize(mod);
    }
    void del (const string& k);
    void put (const string& k, const string& v);
    string get (const string& k);
private:
     vector<list<Pair>> data;
     int count_hash (const string& s);
     void delHash (int hash, const string& k);
};

void HashTable::delHash(int hash, const string& k) {
    for (auto it = data[hash].begin(); it != data[hash].end(); ) {
        if (it->key == k) {
            it = data[hash].erase(it);
            return;
        } 
        else ++it;
    }
    return;
}

void HashTable::del(const string& k) {
    int hash = count_hash(k);
    delHash(hash, k);
}

void HashTable::put (const string& k, const string& v) {
    int hash = count_hash(k);
    delHash(hash, k);
    data[hash].push_back(Pair(k,v));
    return;
}

string HashTable::get (const string& k) {
    int hash = count_hash(k);
    for (auto p:data[hash]) {
        if (p.key == k) {
            return p.value;
        }
    }
    return "";
}

int HashTable::count_hash (const string& k) {
    int hash = 0; 
    int cur = 1;
    for (size_t i = 0; i < k.size();i++) {
        hash += int(k[i])*cur; 
        cur *= p; 
        cur %= mod; 
        hash %= mod;
    }
    return hash;
}

int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);
    std::cout.tie(nullptr);
    freopen("map.in", "r", stdin);
    freopen("map.out", "w", stdout);
    string s,x,y;
    HashTable table;
    while (std::cin >> s) {
        if (s == "put") {
            std::cin >> x >> y;
            table.put(x,y);
        }
        else if (s == "get") {
            std::cin >> x;
            string ans = table.get(x);
            if (ans == "") {
                std::cout << "none" << std::endl;
            }
            else std::cout << ans << std::endl;
        }
        else if (s == "delete") {
            std::cin >> x;
            table.del(x);
        }
    }
    return 0;
}