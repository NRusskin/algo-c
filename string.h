#include <iostream>
#include <cstring>

class String {
public:
    String() {}
    
    String(const char c): size(1), str(new char[4]), bufer(4) {
        str[0] = c;
    }
    
    String(const char * s): size(strlen(s)), str(new char[strlen(s)]), bufer(strlen(s)) {
        memcpy(str, s, size);
    }
    
    String(size_t size, char c);
    
    String(const String & s): size(s.size), str(new char[size]), bufer(s.size) {
        memcpy(str, s.str, size);
    }
    
    ~String() {
        delete[] str;
    }
    
    void push_back(char c);
    
    void pop_back() {
        --size;
    }
    
    String substr(size_t start, size_t count) const; 
    
    size_t find(const String & s) const;

    size_t rfind(const String & s) const;

    size_t length() const {
        return size;
    }

    const char & front() const {
        return *(str);
    }
    
    const char & back() const {
        return *(str + size - 1);
    }
    
    char & front() {
        return *(str);
    }
    
    char & back() {
        return *(str + size - 1);
    }
    
    void clear();
    
    bool empty() const;

    String & operator += (const String & s);
    
    char& operator[](size_t f) {
        return *(str + f);
    }

    char operator[](size_t f) const {
        return *(str + f);
    }

    String& operator = (const String& s);
    
    bool operator == (const String& a) const;
private:
    size_t size = 0;
    char * str = new char[4];
    size_t bufer = 4;
    
    bool equality(size_t index, const String & s) const;
    
    void swap(String & s);    
};

String::String(size_t size, char c): size(size), str(new char[size]), bufer(size) {
    memset(str, c, size);
}

bool String::equality(size_t index, const String & s) const {
    bool equality = true;
    for (size_t i = 0; i < s.length(); ++i) {
        if (str[index + i] != s[i]) {
            equality = false;
            break;
        }
    }
    return equality;
}

size_t String::find(const String & s) const {
    if (s.length() > size) return size;
    for (size_t i = 0; i < size - s.length() + 1; ++i) {
        if (equality(i, s)) return i;
    }
    return size;
}

size_t String::rfind(const String & s) const {
    if (s.length() > size) return size;
    for (size_t i = size - s.length() + 1; i > 0; --i) {
        if (equality(i, s)) return i;
    }
    return size;
}
    
void String::push_back(char c) {
    if (bufer == size) {
        bufer *= 2;
        char * strcopy = new char[bufer];
        memcpy(strcopy, str, size);
        delete[] str;
        str = strcopy;
    }
    str[size] = c;
    ++size;
}

bool String::empty() const {
    if (size == 0)
        return true;
    else
        return false;
}
    
String String::substr(size_t start, size_t count) const {
    String s;
    for (size_t i = 0; i < count; ++i) {
        s.push_back(str[i + start]);
    }
    return s;
}

void String::swap(String & s) {
    std::swap(str, s.str);
    std::swap(size, s.size);
    std::swap(bufer, s.bufer);
}

void String::clear() {
    size = 0;
    delete[] str;
    str = new char[1];
    bufer = 1;
}

String& String::operator = (const String & s) {
    if (this == & s) return *this;
    String newString = s;
    swap(newString);
    return *this;
}

String& String::operator += (const String & s) {
    if (bufer<size+s.size) {
        bufer += s.bufer;
        bufer*=2;
        char* strcopy = new char[bufer];
        memcpy(strcopy, str, size);
        delete[] str;
        for (size_t i=0; i<s.size; ++i) {
	        strcopy[size+i]=s.str[i];
        }
        str = strcopy;
    }
    else  for (size_t i=0; i<s.size; ++i) {
    	str[size+i]=s.str[i];
    }
    size+=s.size;
    return*this;
}

bool String::operator == (const String & a) const {
    if (a.length() != size) return false;
    for (size_t i = 0; i < size; ++i) {
        if (a[i] != str[i]) return false;
    }
    return true;
}

String operator + (const String & a, const String & b) {
    String newString = a;
    newString += b;
    return newString;
}

std::ostream & operator << (std::ostream & out,
    const String & s) {
    for (size_t i = 0; i < s.length(); ++i) out << s[i];
    return out;
}

std::istream & operator >> (std::istream & in , String & s) {
    s.clear();
    char c = '\0';
    while (! in .eof() && isspace(c = in .get())) {}
    while (! in .eof() && !isspace(c)) {
        s.push_back(c);
        c = in .get();
    }
    return in;
}
