/*L. Симпатичные узоры — 3
ограничение по времени на тест3 секунды
ограничение по памяти на тест64 мегабайта
вводстандартный ввод
выводстандартный вывод
Компания BrokenTiles планирует заняться выкладыванием во дворах у состоятельных клиентов узор из черных и белых плиток, каждая из которых имеет размер 1×1 метр.
Известно, что дворы всех состоятельных людей имеют наиболее модную на сегодня форму прямоугольника M×N метров.
Однако при составлении финансового плана у директора этой организации появилось целых две серьезных проблемы:
во первых, каждый новый клиент очевидно захочет, чтобы узор, выложенный у него во дворе, отличался от узоров всех остальных клиентов этой фирмы, а во вторых, этот узор должен быть симпатичным.
Как показало исследование, узор является симпатичным, если в нем нигде не встречается квадрата 2×2 метра, полностью покрытого плитками одного цвета.
Для составления финансового плана директору необходимо узнать, сколько клиентов он сможет обслужить, прежде чем симпатичные узоры данного размера закончатся. Помогите ему!
Входные данные
На первой строке входного файла находятся три натуральных числа n, m, mod. 1≤n≤10100, 1≤m≤6, 1≤mod≤10000.
n, m — размеры двора. mod — модуль, по которому нужно посчитать ответ.
Выходные данные
Выведите в выходной файл единственное число — количество различных симпатичных узоров, которые можно выложить во дворе размера n×m по модулюmod.
Узоры, получающиеся друг из друга сдвигом, поворотом или отражением считаются различными.*/

#include <iostream>
#include <vector>
#include <cmath>

using std::vector;

class Matrix {
public:
    Matrix(size_t t): sz(t), data(t) {
        for (size_t i = 0; i < t; ++i) {
            data[i].resize(t, 0);
            data[i][i] = 1;
        }
    }
    size_t size() const {
        return sz;
    }
    vector<long long>& operator[](long long i) {
        return data[i];
    }
    const vector<long long>& operator[](long long i) const {
        return data[i];
    }
private:
    size_t sz;
    vector<vector<long long>> data;
};

Matrix matrixMultiplication (const Matrix& a, const Matrix& b, long long mod) {
    Matrix tmp(a.size());
    for (size_t i = 0; i < a.size(); ++i) {
        for (size_t j = 0; j < a.size(); ++j) {
            long long p = 0;
            for (size_t k = 0; k < a.size(); ++k) {
                p += (a[i][k] * b[k][j]) % mod;
                p %= mod;
            }
            tmp[i][j] = p;
        }
    }
    return tmp;
}

int sum (const vector<int>& n) {
    int s = 0;
    for (size_t i = 0; i < n.size(); ++i) {
        s += n[i];
    }
    return s;
}

void decrementation(vector<int>& n) {
    int k = n.size() - 1;
    while (n[k] == 0 && k > 0) {
        n[k] = 9;
        --k;
    }
    n[k] = n[k] - 1;
}

void halfDivide(vector<int>& n) {
    bool p = false;
    int tmp;
    for (size_t i = 0; i < n.size(); ++i) {
        tmp = n[i];
        n[i] = p ? (tmp + 10) / 2 : tmp / 2;
        p = tmp % 2 ? true : false;
    }
}

Matrix binpow (Matrix a, vector<int>& n, int mod) {
    Matrix res(a.size());
    while (sum(n)) {
        if (n[n.size() - 1] % 2) {
            res = matrixMultiplication(res, a, mod);
            decrementation(n);
        } 
        else {
            a = matrixMultiplication(a, a, mod);
            halfDivide(n);
        }
    }
    return res;
}

int binpow (vector<int>& n, int mod) {
    int res = 1;
    int a = 2;
    while (sum(n)) {
        if (n[n.size() - 1] % 2) {
            res = res * a % mod;
        }
        a = a * a % mod;
        halfDivide(n);
    }
    return res;
}
 
void check (bool& p, int j, int l, int k) {
    int left_up = (j >> l) & 1;
    int left_down = (k >> l) & 1;
    int right_up = (j >> (l + 1)) & 1;
    int right_down = (k >> (l + 1)) & 1;
    if (left_up == left_down && left_down == right_up && right_up == right_down) {
        p = false; 
    }
}

int countPatterns (int m, vector<int>& n, int mod) {
    if (m == 1) {
        int ans = binpow(n, mod);
        return ans % mod;
    }
    const int maxmask = pow(2, m);
    if (n.size() == 1 && n[0] == '1') {
        return maxmask % mod;
    }
    Matrix ok(maxmask);
    for (int j = 0; j < maxmask; ++j) {
        for (int k = 0; k < maxmask; ++k) {
            bool p = true;
            for (int l = 0; l < m - 1; ++l) {
                check(p, j, l, k);
                ok[j][k] = p ? 1 : 0;
            }
        }
    }
    decrementation(n);
    ok = binpow(ok, n, mod);
    int ans = 0;
    for (size_t i = 0; i < ok.size(); ++i) {
        for (size_t j = 0; j < ok.size(); ++j) {
            ans += ok[i][j];
            ans %= mod;
        }
    }
    return ans;
}

int main() {
    int m, mod;
    std::string s;
    std::cin >> s >> m >> mod;
    vector<int> n;
    for (size_t i = 0; i < s.size(); ++i) {
        n.push_back(s[i] - '0');
    }
    std::cout << countPatterns(m, n, mod);
    return 0;
}