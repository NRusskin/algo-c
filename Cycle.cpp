/*C. Поиск цикла
ограничение по времени на тест1 секунда
ограничение по памяти на тест256 мегабайт
вводстандартный ввод
выводстандартный вывод
Дан ориентированный невзвешенный граф без петель и кратных рёбер. Необходимо определить есть ли в нём циклы, 
и если есть, то вывести любой из них.
Входные данные
В первой строке входного файла находятся два натуральных числа N и M (1⩽N⩽100000, M⩽100000) — 
количество вершин и рёбер в графе соответственно. Далее в M строках перечислены рёбра графа. 
Каждое ребро задаётся парой чисел  — номерами начальной и конечной вершин соответственно.
Выходные данные
Если в графе нет цикла, то вывести «NO», иначе  — «YES» и затем перечислить все вершины в порядке обхода цикла.*/

#include <iostream>
#include <vector>
#include <algorithm>

using std::vector;
using std::cin;
using std::cout;
 
class CGraph {
public:
    void AddEdge(int from, int to);
    int VerticesCount() const;
    const vector<int>& GetNextVertices(int vertex) const;
    explicit CGraph(int count): adjacencyList(count) {}
private:
    vector<vector<int>> adjacencyList;
};
 
void CGraph::AddEdge(int from, int to) {
    adjacencyList[from].push_back(to);
}
 
int CGraph::VerticesCount() const {
    return adjacencyList.size();
}
 
const vector<int>& CGraph::GetNextVertices(int vertex) const {
    return adjacencyList[vertex];
}
 
enum Status {
    VISITED,
    ENTERED,
    NOT_VISITED
};

bool findCycle(int v, vector<Status>& status, vector<int>& parent, const CGraph& graph, 
int& endCycle, int& start) {
    status[v] = ENTERED;
    for (auto t: graph.GetNextVertices(v)) {
        if (status[t] == NOT_VISITED) {
            parent[t] = v;
            if (findCycle(t, status, parent, graph, endCycle, start)) {
                return true;
            }  
        }
        else if (status[t] == ENTERED) {
            endCycle = v;
            start = t;
            return true;
        }
    }
    status[v] = VISITED;
    return false;
}

bool findCycle(vector<int>& cycle, const CGraph& graph) {
    int n = graph.VerticesCount();
    vector<Status> status(n, NOT_VISITED);
    vector<int> parent (n, -1);
    int start = -1, endCycle;
    bool hasCycle = false;
    for (int i = 0; i < n; ++i) {
        if (findCycle(i, status, parent, graph, endCycle, start)) {
            hasCycle = true;
            break;
        }
    }
    if (!hasCycle) {
       return false;
    }
    else {
        cycle.push_back(start);
        for (int v = endCycle; v != start; v = parent[v]) {
            cycle.push_back(v);
        }
        std::reverse(cycle.begin(), cycle.end());
    }
    return true;
}

int main() {
    int n, m, x, y;
    cin >> n >> m;
    CGraph graph(n);
    for (int i = 0; i < m; ++i) {
        cin >> x >> y;
        --x;
        --y;
        graph.AddEdge(x, y);
    }
    std::vector<int> cycle;
    if (findCycle(cycle, graph)) {
        cout << "YES" << std::endl;
        for (auto ans: cycle) {
            cout << ans + 1 << " ";
        }
    }
    else {
        cout << "NO";
    }
}