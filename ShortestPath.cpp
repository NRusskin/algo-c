/*F. Расстояние между вершинами
ограничение по времени на тест2 секунды
ограничение по памяти на тест256 мегабайт
вводстандартный ввод
выводстандартный вывод
Дан взвешенный неориентированный граф. Требуется найти вес минимального пути между двумя вершинами.
Входные данные
Первая строка входного файла содержит два натуральных числа n и m — количество вершин и ребер графа соответственно. 
Вторая строка входного файла содержит натуральные числа s и t — номера вершин, длину пути между которыми требуется найти (1≤s,t≤n, s≠t).
Следующие m строк содержат описание ребер по одному на строке. Ребро номер i описывается тремя натуральными 
числами bi, ei и wi — номера концов ребра и его вес соответственно (1≤bi,ei≤n, 0≤wi≤100).
n≤100000, m≤200000.
Выходные данные
Первая строка выходного файла должна содержать одно натуральное число — вес минимального пути между вершинами s и t.
Если путь из s в t не существует, выведите -1.*/

#include <iostream>
#include <set>
#include <vector>
#include <limits>

using std::vector;
using std::cin;
using std::cout;
using std::pair;
using std::make_pair;
using std::numeric_limits;
using std::set;

class CGraph {
public:
    explicit CGraph(int count): adjacencyList(count) {}
    void AddEdge(int from, int to, int weight);
    int VerticesCount() const;
    const vector<pair<int, int>>& GetNextVertices(int vertex) const;
private:
    vector<vector<pair<int, int>>> adjacencyList;
};

void CGraph::AddEdge(int from, int to, int weight) {
    adjacencyList[from].push_back(make_pair(to, weight));
    adjacencyList[to].push_back(make_pair(from, weight));
}

int CGraph::VerticesCount() const {
    return adjacencyList.size();
}

const vector<pair<int, int>>& CGraph::GetNextVertices(int vertex) const {
    return adjacencyList[vertex];
}

int shortestPath(int s, int t, const CGraph& graph) {
    int n = graph.VerticesCount();
    vector<int> dp(n, numeric_limits<int>::max()), p(n);
    dp[s] = 0;
    set<pair<int, int>> que;
    que.insert(make_pair(dp[s], s));
    while (!que.empty()) {
        int v = que.begin() -> second;
        que.erase(que.begin());
        for (auto f: graph.GetNextVertices(v)) {
            int to = f.first;
            int path = f.second;
            if (dp[v] + path < dp[to]) {
                que.erase(make_pair(dp[to], to));
                dp[to] = dp[v] + path;
                p[to] = v;
                que.insert(make_pair(dp[to], to));
            }
        }
    }
    if (dp[t] == numeric_limits<int>::max()) {
        return -1;
    }
    else {
        return dp[t];
    }
}

int main() {
    int n, m, s, t, from, to, weight;
    cin >> n >> m;
    cin >> s >> t;
    --s, --t;
    CGraph graph(n);
    for (int i = 0; i < m; ++i) {
        cin >> from >> to >> weight;
        --from, --to;
        graph.AddEdge(from, to, weight);
    }
    cout << shortestPath(s, t, graph);
}