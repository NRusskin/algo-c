#include <iostream>
#include <vector>
#include <string>

class BigInteger {
public:
    BigInteger(const std::vector < long long > & digits, const int & size, const bool & nonpositive): digits(digits), size(size), nonpositive(nonpositive) {}

    BigInteger(): digits(std::vector < long long > (1)), size(1), nonpositive(false) {}

    BigInteger(const BigInteger & a): digits(a.digits), size(a.size), nonpositive(a.nonpositive) {}

    BigInteger(const int & a);
    
    friend bool operator < (const BigInteger & a, const BigInteger & b);
    friend std::istream & operator >> (std::istream & in , BigInteger & a);
    
    size_t length() const {
        return size;
    }

    BigInteger abs() const {
        BigInteger b = * this;
        b.nonpositive = false;
        return b;
    }
    
    explicit operator bool() const {
        if (size > 1) return true;
        return digits[0];
    }
    
    explicit operator int() const;
    
    void swap(BigInteger & number);

    static void swap(BigInteger & first, BigInteger & second);

    static int count_digits(int x);

    static void filling_zeros(BigInteger & a, int n);

    BigInteger & operator = (const BigInteger & a) {
        BigInteger b = a;
        swap(b);
        return *this;
    }

    BigInteger operator - () const;
    
    BigInteger & operator += (const BigInteger & a);
    
    BigInteger & operator -= (const BigInteger & a) {
        return *this += (-a);
    }
    
    BigInteger & operator *= (const BigInteger & a);

    BigInteger & operator /= (const BigInteger & b);

    BigInteger & operator %= (const BigInteger & a);

    BigInteger & operator++();

    BigInteger operator++(int) {
        BigInteger copy( * this);
        ++( * this);
        return copy;
    }

    BigInteger operator--(int) {
        BigInteger copy( * this);
        --( * this);
        return copy;
    }

    BigInteger& operator--();

    std::string toString() const;
    
private:
    std::vector < long long > digits;
    size_t size;
    bool nonpositive;
    friend class Rational;
    void module_increment();
    void module_decrement();
};

BigInteger::BigInteger(const int & a): digits(std::vector < long long > (1)), size(1), nonpositive(false){
    int b = a;
    if (b < 0) {
        nonpositive = true;
        b *= -1;
    }
    digits[0] = b % 10;
    b /= 10;
    while (b != 0) {
        digits.push_back(b % 10);
        ++size;
        b /= 10;
    }
}

BigInteger::operator int() const {
    int x = digits[0];
    for (size_t i = 1; i < size; ++i) {
        x *= 10;
        x += digits[i];
    }
    if (nonpositive) x *= -1;
    return x;
}
    
void BigInteger::swap(BigInteger & number) {
    std::swap(number.nonpositive, nonpositive);
    std::swap(number.digits, digits);
    std::swap(number.size, size);
}

void BigInteger::swap(BigInteger & first, BigInteger & second) {
    BigInteger tmp = first;
    first = second;
    second = tmp;
}

int BigInteger::count_digits(int x) {
    int count = 0;
    while (x != 0) {
        ++count;
        x /= 10;
    }
    return count;
}

void BigInteger::filling_zeros(BigInteger & a, int n) {
    for (int i = 0; i < n; ++i) {
        a.digits.push_back(0);
        ++a.size;
    }
}

void BigInteger::module_increment() {
    ++digits[0];
    size_t pointer = 0;
    while (digits[pointer] == 10) {
        digits[pointer] = 0;
        ++pointer;
        if (size == pointer) {
            digits.push_back(1);
            ++size;
            break;
        } else {
            ++digits[pointer];
        }
    }
}

void BigInteger::module_decrement() {
    --digits[0];
    size_t pointer = 0;
    while (digits[pointer] == -1) {
        digits[pointer] += 10;
        ++pointer;
        if (pointer < size) {
            --digits[pointer];
        }
    }
    pointer = size - 1;
    while (size > 1 && !digits[pointer]) {
        --size;
        digits.pop_back();
    }
}

std::ostream & operator << (std::ostream & out,
    const BigInteger & a) {
    std::string s = a.toString();
    out << s;
    return out;
}

bool operator < (const BigInteger & a, const BigInteger & b) {
    if (a.nonpositive != b.nonpositive) return a.nonpositive;
    int farrow = 0;
    if (a.nonpositive) farrow = 1;
    if (a.size < b.size) return true ^ farrow;
    if (a.size > b.size) return false ^ farrow;
    for (int i = a.size - 1; i >= 0; --i) {
        if (a.digits[i] < b.digits[i]) {
            return true ^ farrow;
        } else if (a.digits[i] > b.digits[i]) {
            return false ^ farrow;
        }
    }
    return false;
}

bool operator > (const BigInteger & a,const BigInteger & b) {
    return b < a;
}

bool operator <= (const BigInteger & a,const BigInteger & b) {
    return !(a > b);
}
bool operator >= (const BigInteger & a,const BigInteger & b) {
    return !(a < b);
}
bool operator == (const BigInteger & a,const BigInteger & b) {
    return a <= b && b <= a;
}

BigInteger & BigInteger::operator += (const BigInteger & a) {
    BigInteger b = a;
    if (b.size < size) filling_zeros(b, size - b.size);
    else filling_zeros( * this, b.size - size);
    if (nonpositive == b.nonpositive) {
        for (size_t i = 0; i < b.size && i < size; ++i) {
            digits[i] += b.digits[i];
            if (digits[i] >= 10 && i == size - 1) {
                digits.push_back(1);
                ++size;
            } else if (digits[i] >= 10) {
                ++digits[i + 1];
            }
            digits[i] %= 10;
        }
    } else {
        if (abs() < b.abs()) {
            swap( * this, b);
            nonpositive = !b.nonpositive;
        }
        if (abs() == b.abs()) {
            nonpositive = false;
        }
        for (size_t i = 0; i < b.size && i < size; ++i) {
            digits[i] -= b.digits[i];
            if (digits[i] < 0) {
                digits[i] += 10;
                --digits[i + 1];
            }
        }
    }
    while (size > 1 && digits[size - 1] == 0) {
        --size;
        digits.pop_back();
    }
    return *this;
}

BigInteger & BigInteger::operator--() {
    if ( * this == 0) {
        nonpositive = true;
        digits[0] = 1;
        return *this;
    }
    if (nonpositive) module_increment();
    else module_decrement();
    return *this;
}

std::istream & operator >> (std::istream & in , BigInteger & a) {
    std::string s; in >> s;
    if (s[0] == '-') {
        a.nonpositive = true;
        s.erase(0, 1);
    } else {
        a.nonpositive = false;
    }
    a.digits.clear();
    a.size = 0;
    for (size_t i = 0; i < s.length(); ++i) {
        if (i < (s.length() - 1)) {
            a.digits.push_back(stoi(s.substr(s.length() - (i + 1), 1)));
        } else a.digits.push_back(stoi(s.substr(0, 1)));
    }
    a.size = a.digits.size();
    return in;
}

BigInteger operator + (const BigInteger & a, const BigInteger & b) {
    BigInteger x(a);
    x += b;
    return x;
}

BigInteger operator - (const BigInteger & a, const BigInteger & b) {
    BigInteger x(a);
    x -= b;
    return x;
}

BigInteger & BigInteger::operator *= (const BigInteger & a) {
    BigInteger compose;
    if (a == 0 || * this == 0) {
        compose.nonpositive = false;
        compose = 0;
        * this = compose;
        return *this;
    } else if (a.nonpositive != nonpositive) {
        compose.nonpositive = true;
    } else {
        compose.nonpositive = false;
    }
    compose.size = a.size + size;
    compose.digits.resize(a.size + size);
    for (size_t i = 0; i < a.size; ++i) {
        for (size_t j = 0; j < size; ++j) {
            compose.digits[i + j] += a.digits[i] * digits[j];
        }
    }
    for (size_t i = 0; i < compose.size; ++i) {
        if (compose.digits[i] >= 10) {
            compose.digits[i + 1] += compose.digits[i] / 10;
            compose.digits[i] %= 10;
        }
    }
    while (compose.size > 1 && compose.digits[compose.size - 1] == 0) {
        --compose.size;
    }
    * this = compose;
    return *this;
}

BigInteger operator * (const BigInteger & a,const BigInteger & b) {
    BigInteger compose = a;
    compose *= b;
    return compose;
}

BigInteger & BigInteger::operator /= (const BigInteger & b) {
    BigInteger module = b.abs();
    BigInteger c;
    if (abs() < module) {
        * this = c;
        return *this;
    }
    std::vector < int > tmp;
    BigInteger aux = 0;
    int l, r, middle, matatag;
    for (size_t i = 0; i < size; ++i) {
        aux += digits[size - i - 1];
        l = 0, r = 10 - 1;
        matatag = 10;
        while (l <= r) {
            middle = (l + r) / 2;
            if (middle * module <= aux) {
                matatag = middle;
                l = middle + 1;
            } else {
                r = middle - 1;
            }
        }
        aux -= matatag * module;
        aux *= 10;
        tmp.push_back(matatag);
    }
    c.size = 0;
    c.digits.clear();
    for (size_t i = 0; i < tmp.size(); ++i) {
        c.digits.push_back(tmp[tmp.size() - i - 1]);
        ++c.size;
    }
    while (c.size > 1 && c.digits[c.size - 1] == 0) {
        --c.size;
        c.digits.pop_back();
    }
    if (nonpositive != b.nonpositive) c.nonpositive = true;
    * this = c;
    return *this;
}

BigInteger operator / (const BigInteger & a,const BigInteger & b) {
    BigInteger compose = a;
    compose /= b;
    return compose;
}

BigInteger & BigInteger::operator %= (const BigInteger & a) {
    * this -= ( * this / a) * a;
    return *this;
}

BigInteger operator % (const BigInteger & a, const BigInteger & b) {
    BigInteger c(a);
    c %= b;

    return c;
}

BigInteger BigInteger::operator - () const {
    if ( * this == 0) return *this;
    return BigInteger(digits, size, !nonpositive);
}

BigInteger & BigInteger::operator++() {
    if ( * this == -1) {
        nonpositive = false;
        digits[0] = 0;
        return *this;
    }
    if (nonpositive) module_decrement();
    else module_increment();
    return *this;
}

bool operator != (const BigInteger & a, const BigInteger & b) {
    return !(a == b);
}

std::string BigInteger::toString() const {
    std::string s;
    if (nonpositive) s += '-';
    for (size_t i = 0; i < size; ++i) {
        if (i) {
            for (int j = 0; j < 1 - count_digits(digits[size - 1 - i]); ++j) {
                s += '0';
            }
        }
        if (digits[size - 1 - i] != 0) {
            s += std::to_string(digits[size - 1 - i]);
        } else if (size == 1) {
            s += '0';
        }
    }
    return s;
}
    
class Rational {
public:
    Rational(const int & a) {
        * this = Rational(BigInteger(a));
    }

    Rational(const BigInteger & a): m(a), n(1) {}

    Rational(const BigInteger & p,const BigInteger & q): m(p), n(q) {
        irreducible();
    }

    Rational(): m(0), n(1) {}

    Rational(const Rational & a): m(a.m), n(a.n) {}
    friend bool operator < (const Rational & a,const Rational & b);

    explicit operator double();

    Rational & operator = (const Rational & a) {
        Rational b = a;
        swap(b);
        return *this;
    }

    Rational operator - () const {
        return Rational(-m, n);
    }

    Rational & operator += (const Rational & a);

    Rational & operator -= (const Rational & a) {
        * this += (-a);
        return *this;
    }

    Rational & operator *= (const Rational & a);

    Rational & operator /= (const Rational & a);

    std::string toString() const {
        if (n == 1) return m.toString();
        return m.toString() + '/' + n.toString();
    }

    std::string asDecimal(size_t prec = 0) const;
    
private:
    BigInteger m;
    BigInteger n;
    
    static BigInteger gcd(BigInteger a, BigInteger b) {
        if (b) return gcd(b, a % b);
        else return a;
    }
    
    void irreducible();
    
    void swap(Rational & a) {
        std::swap(a.m, m);
        std::swap(a.n, n);
    }
};

Rational::operator double() {
    std::string s = asDecimal(50);
    char * str = new char[s.size()];
    for (size_t i = 0; i < s.size(); ++i) {
        str[i] = s[i];
    }
    return std::strtod(str, nullptr);
}
    
void Rational::irreducible() {
    if (m == 0) {
        n = 1;
        return;
    }
    BigInteger d = gcd(m, n).abs();
    m /= d;
    n /= d;
}
    
Rational & Rational::operator *= (const Rational & a) {
    m *= a.m;
    n *= a.n;
    irreducible();
    return *this;
}

Rational operator * (const Rational & a,
    const Rational & b) {
    Rational c(a);
    c *= b;
    return c;
}

bool operator < (const Rational & a,
    const Rational & b) {
    return a.m * b.n < b.m * a.n;
}

bool operator > (const Rational & a,
    const Rational & b) {
    return b < a;
}

bool operator <= (const Rational & a,
    const Rational & b) {
    return !(a > b);
}

bool operator >= (const Rational & a,
    const Rational & b) {
    return !(a < b);
}

bool operator == (const Rational & a,
    const Rational & b) {
    return a <= b && b <= a;
}

bool operator != (const Rational & a,
    const Rational & b) {
    return !(a == b);
}

Rational & Rational::operator /= (const Rational & a) {
    m *= a.n;
    n *= a.m;
    if (n < 0) {
        n *= (-1);
        m *= (-1);
    }
    irreducible();
    return *this;
}

Rational operator / (const Rational & a,
    const Rational & b) {
    Rational c(a);
    c /= b;
    return c;
}

Rational & Rational::operator += (const Rational & a) {
    m = m * a.n + a.m * n;
    n *= a.n;
    irreducible();
    return *this;
}

Rational operator + (const Rational & a,
    const Rational & b) {
    Rational c(a);
    c += b;
    return c;
}

Rational operator - (const Rational & a,
    const Rational & b) {
    Rational c(a);
    c += (-b);
    return c;
}

std::string Rational::asDecimal(size_t prec) const {
    std::string ring;
    std::string t = "";
    ring = (m.abs() / n).toString();
    if (ring[0] == '-') ring.erase(0, 1);
    int add = ring.size();
    if (ring[0] == '0') add = 0;
    ring.clear();
    if (m.nonpositive) {
        t = '-';
    }
    Rational tmp = * this;
    tmp.m = tmp.m.abs();
    std::string str;
    for (size_t i = 0; i < prec + add; ++i) {
        tmp.m *= 10;
        if (tmp.m < tmp.n) {
            str += '0';
        }
    }
    BigInteger c = tmp.m / tmp.n;
    ring = c.toString();
    std::string pstr;
    if (prec > 0) pstr += '.';
    if (m.abs() >= n) {
        return t + ring.substr(0, add) + pstr + ring.substr(add, prec);
    }
    if (ring == "0") ring = "";
    return t + "0" + pstr + str + ring;
}
