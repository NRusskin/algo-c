#include <vector>
#include <iostream>
#include <type_traits>
#include <iterator>
#include <vector>
#include <cassert>

using std::pair;
using std::make_pair;
using std::vector;

template <typename T, typename Allocator = std::allocator <T>>
class List {
private:
    template <bool IsConst>
    class common_iterator;
public:
    using iterator = common_iterator <false> ;
    using const_iterator = common_iterator <true> ;
    using reverse_iterator = std::reverse_iterator <iterator> ;
    using const_reverse_iterator = std::reverse_iterator <const_iterator>;
    explicit List(const Allocator & alloc = Allocator());
    explicit List(size_t count, const T& value, const Allocator& alloc = Allocator());
    explicit List(size_t count, const Allocator& alloc = Allocator());
    List(const List<T, Allocator>& lst);
    List(List&& lst);

    List& operator=(List&& lst);

    template<typename... Args>
    void emplace_back(Args&&... args);

    ~List();

    void push_front(const T& value);

    void pop_front();

    void push_back(const T& value);

    void push_back();

    void pop_back();

    size_t size() const;

    Allocator get_allocator() const;

    List& operator = (const List<T, Allocator>& lst);

    iterator begin() const {
        return iterator(tale -> next);
    }

    iterator end() const {
        return iterator(tale);
    }

    const_iterator cbegin() const {
        return const_iterator(tale -> next);
    }

    const_iterator cend() const {
        return const_iterator(tale);
    }

    reverse_iterator rbegin() const {
        return reverse_iterator(tale);
    }

    reverse_iterator rend() const {
        return reverse_iterator(tale -> next);
    }

    const_reverse_iterator crbegin() const {
        return const_reverse_iterator(tale);
    }

    const_reverse_iterator crend() const {
        return const_reverse_iterator(tale -> next);
    }

    void insert(const const_iterator& it, const T& value) {
        Node* tmp = const_cast <Node*> (it.node);
        insert_before(tmp, value);
    }

    void erase(const const_iterator& it) {
        Node* tmp = const_cast <Node*> (it.node);
        erase(tmp);
    }
private:
    struct Node {
        Node* prev = nullptr;
        Node* next = nullptr;
        T value;
        Node(const T& val): value(val) {}
        Node() = default;
    };
    size_t sz = 0;
    Node* tale;
    Allocator alloc;
    typename std::allocator_traits<Allocator>::template rebind_alloc<Node> NodeAlloc;
    using NodeAllocTraits = std::allocator_traits <decltype(NodeAlloc)>;
    void initTale() {
        tale = NodeAlloc.allocate(1);
        tale -> next = tale;
        tale -> prev = tale;
    }

    void erase(Node* node);

    Node* createNode(const T& value) {
        Node* newNode = NodeAlloc.allocate(1);
        NodeAllocTraits::construct(NodeAlloc, newNode, value);
        return newNode;
    }
    Node* createNode() {
        Node* newNode = NodeAlloc.allocate(1);
        NodeAllocTraits::construct(NodeAlloc, newNode);
        return newNode;
    }

    void insert_before(Node* node, const T& value);
    template <bool IsConst>
    class common_iterator {
    public:
        std::conditional_t<IsConst, const Node* , Node*> node;
        using TT = std::conditional_t <IsConst,
                const T, T> ;
        using iterator_category = std::bidirectional_iterator_tag;
        using value_type = TT;
        using reference = TT&;
        using pointer = TT*;
        using difference_type = std::ptrdiff_t;
        common_iterator(): node(nullptr) {}
        common_iterator(std::conditional_t<IsConst, const Node* , Node*> t): node(t) {}
        common_iterator(const common_iterator& it): node(it.node) {}
        operator common_iterator <true> () {
            return common_iterator <true> (node);
        }
        common_iterator& operator++() {
            node = node -> next;
            return *this;
        }

        common_iterator& operator--() {
            if (node != nullptr) {
                node = node -> prev;
            }
            return *this;
        }

        common_iterator operator++(int) {
            common_iterator copy(*this);
            ++( * this);
            return copy;
        }

        common_iterator operator--(int) {
            common_iterator copy(*this);
            --(*this);
            return copy;
        }

        std::conditional_t <IsConst,
                const T&, T&> operator* () const {
            return node -> value;
        }

        bool operator == (common_iterator it) {
            return node == it.node;
        }

        bool operator != (common_iterator it) {
            return node != it.node;
        }

        std::conditional_t <IsConst,
                const T*, T*> operator -> () const {
            return &(node -> value);
        }

        common_iterator & operator = (const common_iterator <false>& it) {
            node = it.node;
            return *this;
        }
    };
};

template<typename T, typename Allocator>
List<T, Allocator>::List(const Allocator &alloc): NodeAlloc(alloc) {
    initTale();
}

template<typename T, typename Allocator>
List<T, Allocator>::List(size_t count, const T& value, const Allocator& alloc): NodeAlloc(alloc) {
    initTale();
    for (size_t i = 0; i < count; ++i) {
        push_back(value);
    }
}

template<typename T, typename Allocator>
List<T, Allocator>::List(size_t count, const Allocator& alloc): NodeAlloc(alloc) {
    initTale();
    for (size_t i = 0; i < count; ++i) {
        push_back();
    }
}

template<typename T, typename Allocator>
List<T, Allocator>::List(const List<T, Allocator>& lst) {
    NodeAlloc = NodeAllocTraits::select_on_container_copy_construction(lst.NodeAlloc);
    initTale();
    Node* tmp = lst.tale -> next;
    while (tmp != lst.tale) {
        push_back(tmp -> value);
        tmp = tmp -> next;
    }
}

template<typename T, typename Allocator>
List<T, Allocator>::~List() {
    Node* tmp = tale -> next;
    while (tmp != tale) {
        Node* tempor = tmp -> next;
        std::allocator_traits<decltype(NodeAlloc)> ::destroy(NodeAlloc, tmp);
        NodeAlloc.deallocate(tmp, 1);
        tmp = tempor;
    }
    NodeAlloc.deallocate(tale, 1);
}

template<typename T, typename Allocator>
void List<T, Allocator>::push_front(const T& value) {
    insert_before(tale -> next, value);
}

template<typename T, typename Allocator>
void List<T, Allocator>::pop_front() {
    erase(tale -> next);
}

template<typename T, typename Allocator>
void List<T, Allocator>::push_back(const T& value) {
    Node* newNode = createNode(value);
    tale -> prev -> next = newNode;
    newNode -> prev = tale -> prev;
    newNode -> next = tale;
    tale -> prev = newNode;
    ++sz;
}

template<typename T, typename Allocator>
void List<T, Allocator>::push_back() {
    Node* newNode = createNode();
    tale -> prev -> next = newNode;
    newNode -> prev = tale -> prev;
    newNode -> next = tale;
    tale -> prev = newNode;
    ++sz;
}

template<typename T, typename Allocator>
void List<T, Allocator>::pop_back() {
    if (sz > 0) {
        erase(tale -> prev);
    }
}

template<typename T, typename Allocator>
size_t List<T, Allocator>::size() const {
    return sz;
}

template<typename T, typename Allocator>
Allocator List<T, Allocator>::get_allocator() const {
    return NodeAlloc;
}

template<typename T, typename Allocator>
List<T,Allocator>& List<T, Allocator>::operator=(const List<T, Allocator>& lst) {
    int size = sz;
    for (int i = 0; i < size; ++i) {
        pop_back();
    }
    if (NodeAllocTraits::propagate_on_container_copy_assignment::value && NodeAlloc != lst.NodeAlloc) {
        NodeAlloc = lst.NodeAlloc;
    }
    Node* tmp = lst.tale -> next;
    while (tmp != lst.tale) {
        push_back(tmp -> value);
        tmp = tmp -> next;
    }
    return *this;
}

template<typename T, typename Allocator>
void List<T, Allocator>::erase(List::Node* node) {
    node -> prev -> next = node -> next;
    node -> next -> prev = node -> prev;
    std::allocator_traits <decltype(NodeAlloc)>::destroy(NodeAlloc, node);
    NodeAlloc.deallocate(node, 1);
    --sz;
}

template<typename T, typename Allocator>
void List<T, Allocator>::insert_before(List::Node* node, const T& value) {
    Node* newNode = createNode(value);
    node -> prev -> next = newNode;
    newNode -> prev = node -> prev;
    node -> prev = newNode;
    newNode -> next = node;
    ++sz;
}

template<typename T, typename Allocator>
List<T, Allocator>::List(List&& lst) {
    sz = lst.sz;
    initTale();
    NodeAlloc = std::move(lst.NodeAlloc);
    lst.sz = 0;
    std::swap(tale, lst.tale);
}

template<typename T, typename Allocator>
List<T, Allocator> &List<T, Allocator>::operator=(List&& lst) {
    if (this ==& lst) {
        return *this;
    }
    int size = sz;
    for (int i = 0; i < size; ++i) {
        pop_back();
    }
    sz = lst.sz;
    tale = lst.tale;
    lst.sz = 0;
    lst.initTale();
    return *this;
}

template<typename T, typename Allocator>
template<typename... Args>
void List<T, Allocator>::emplace_back(Args&& ... args) {
    Node* newNode = NodeAlloc.allocate(1);
    alloc.construct(&newNode->value, std::forward<Args>(args)...);
    tale->prev->next = newNode;
    newNode->prev = tale->prev;
    newNode->next = tale;
    tale->prev = newNode;
    ++sz;
}

template <
        typename Key,
        typename Value,
        typename Hash = std::hash<Key>,
        typename Equal = std::equal_to<Key>,
        typename Allocator = std::allocator<pair<const Key, Value>>
>
class UnorderedMap {
public:
    using NodeType = pair<const Key, Value>;
private:
    using listIterator = typename List<NodeType, Allocator>::iterator;
    using constListIterator = typename List<NodeType, Allocator>::const_iterator;
    using allocIterator = typename std::allocator_traits<Allocator>::template rebind_alloc<listIterator>;
    using dataAlloc = typename std::allocator_traits<Allocator>::template rebind_alloc<vector<listIterator, allocIterator>>;
public:
    using Iterator = listIterator;
    using ConstIterator = constListIterator;

    UnorderedMap():data(1) {}
    UnorderedMap(const UnorderedMap& map) : lst(map.lst) {
        intermix();
    }
    UnorderedMap& operator = (const UnorderedMap& map) {
        lst = map.lst;
        intermix();
        return *this;
    }
    UnorderedMap(UnorderedMap&& map): data(std::move(map.data)), lst(std::move(map.lst)) {}

    UnorderedMap& operator = (UnorderedMap&& map) {
        lst = std::move(map.lst);
        data = std::move(map.data);
        return *this;
    }

    Value& operator[] (const Key& key);

    Value& at(const Key& key);
    
    template<typename... Args>
    pair<Iterator, bool> emplace(Args&&... args);
   
    pair<Iterator, bool> insert(const NodeType& node);

    pair<Iterator, bool> insert(NodeType&& node) {
        return emplace(std::forward<NodeType>(node));
    }
    
    template<typename InputIterator>
    void insert(InputIterator from, InputIterator to) {
        for (InputIterator begin = from; begin != to; ++begin) {
            emplace(*begin);
        }
    }
    
    template<typename T>
    pair<Iterator, bool> insert(T&& value) {
        return emplace(std::forward<T>(value));
    }
    void erase(ConstIterator it);

    void erase(ConstIterator from, ConstIterator to) {
        for (auto it = from; it!= to; ++it) {
            erase(it);
        }
    }

    Iterator find(const Key& key) const;
    
    Iterator begin() const {
        return Iterator(lst.begin());
    }

    Iterator end() const {
        return Iterator(lst.end());
    }

    ConstIterator cbegin() const {
        return ConstIterator(lst.begin());
    }

    ConstIterator cend() const {
        return ConstIterator(lst.end());
    }

    size_t size() const {
        return lst.size();
    }

    void reserve(size_t volume) {
        if (data.size() >= volume) {
            return;
        }
        data.resize(volume);
        intermix();
    }

    size_t max_size() {
        return lst.size()/maxFactor;
    }

    double load_factor() {
        double load_factor = lst.size() / data.size();
        return load_factor;
    }

    double maxLoadFactor() {
        return maxFactor;
    }

private:
    vector<vector<listIterator, allocIterator>, dataAlloc> data;
    List<NodeType, Allocator> lst;
    const double maxFactor = 0.75;
    size_t index(const Key& key) const{
        return Hash()(key) % data.size();
    }
    
    void intermix();

    void checkLoad(){
        if (data.size()*maxFactor < lst.size()) {
            intermix();
        }
    }
};

template<typename Key, typename Value, typename Hash, typename Equal, typename Allocator>
Value& UnorderedMap<Key, Value, Hash, Equal, Allocator>::operator[](const Key &key) {
    Iterator it = find(key);
    if (it == lst.end()) {
        return emplace(make_pair(key, Value())).first->second;
    }
    return it->second;
}

template<typename Key, typename Value, typename Hash, typename Equal, typename Allocator>
Value &UnorderedMap<Key, Value, Hash, Equal, Allocator>::at(const Key &key) {
    Iterator it = find(key);
    if (it == lst.end()) {
        throw std::out_of_range("...");
    }
    return it->second;
}

template<typename Key, typename Value, typename Hash, typename Equal, typename Allocator>
template<typename... Args>
pair<typename UnorderedMap<Key, Value, Hash, Equal, Allocator>::Iterator, bool> UnorderedMap<Key, Value, Hash, Equal, Allocator>::emplace(Args &&... args) {
    lst.emplace_back(std::forward<Args>(args)...);
    Iterator it = lst.end();
    --it;
    Iterator iter = find(it->first);
    if (iter != lst.end()) {
        lst.pop_back();
        return make_pair(iter, false);
    }
    data[index(it->first)].push_back(it);
    checkLoad();
    return make_pair(it, true);
}

template<typename Key, typename Value, typename Hash, typename Equal, typename Allocator>
pair<typename UnorderedMap<Key, Value, Hash, Equal, Allocator>::Iterator, bool> UnorderedMap<Key, Value, Hash, Equal, Allocator>::insert(const UnorderedMap::NodeType &node) {
    Iterator it = find(node->first);
    if (it != lst.end()) {
        return make_pair(it, false);
    }
    lst.push_back(node);
    data[index(it->first)].push_back(it);
    checkLoad();
    return make_pair(it, true);
}

template<typename Key, typename Value, typename Hash, typename Equal, typename Allocator>
void UnorderedMap<Key, Value, Hash, Equal, Allocator>::erase(UnorderedMap::ConstIterator it) {
    size_t idx = index(it->first);
    for (auto& elem: data[idx]) {
        if (Equal()(elem->first, it->first)) {
            std::swap(elem, data[idx].back());
            data[idx].pop_back();
            break;
        }
    }
    lst.erase(it);
}

template<typename Key, typename Value, typename Hash, typename Equal, typename Allocator>
typename UnorderedMap<Key, Value, Hash, Equal, Allocator>::Iterator UnorderedMap<Key, Value, Hash, Equal, Allocator>::find(const Key &key) const {
    if (data.size() == 0) {
        return lst.end();
    }
    for (auto elem: data[index(key)]) {
        if (Equal()(elem->first, key)) {
            return elem;
        }
    }
    return lst.end();
}

template<typename Key, typename Value, typename Hash, typename Equal, typename Allocator>
void UnorderedMap<Key, Value, Hash, Equal, Allocator>::intermix() { // Данный метод расширяет таблицу до нужного ра3мера, перераспределяя все элементы 3аново
    for (size_t i = 0; i < data.size(); ++i) {
        data[i].clear();
    }
    while (data.size() * maxFactor < lst.size()) {
        data.resize(2 * (data.size() + 1));
    }
    for (auto it = lst.begin(); it != lst.end(); ++it) {
        data[index(it->first)].push_back(it);
    }
}
