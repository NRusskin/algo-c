/*B. Невозрастающая подпоследовательность
ограничение по времени на тест2 секунды
ограничение по памяти на тест256 мегабайт
вводстандартный ввод
выводстандартный вывод
Вам требуется написать программу, которая по заданной последовательности находит максимальную невозрастающую её подпоследовательность 
(т.е такую последовательность чисел ai1,ai2,…,aik (i1<i2<…<ik), что ai1≥ai2≥…≥aik и не существует последовательности с теми же свойствами длиной k+1).
Входные данные
В первой строке задано число n — количество элементов последовательности (1≤n≤239017). В последующих строках идут сами числа последовательности ai, 
отделенные друг от друга произвольным количеством пробелов и переводов строки (все числа не превосходят по модулю 231−2).
Выходные данные
Вам необходимо выдать в первой строке выходного файла число k — длину максимальной невозрастающей подпоследовательности. 
В последующих строках должны быть выведены (по одному числу в каждой строке) все номера элементов исходной последовательности ij, 
образующих искомую подпоследовательность. Номера выводятся в порядке возрастания. Если оптимальных решений несколько, разрешается выводить любое.*/

#include <iostream>
#include <vector>
#include <limits>
#include <algorithm>
#include <map>

using std::vector;
using std::map;

int bin_search(int a, int n, const vector<int> &dp);

vector<int> findLIS (const vector<int>& a) {
    int n = a.size();
    vector<int> dp (n+1), pos(n+1), prev(n); // на dp[i] оканчивается невозр послед длины i, наибольшее, если несколько таких
    int length = 0;
    pos[0] = -1;
    dp[0] = std::numeric_limits<int>::min(); //dp[i−1]⩽dp[i] , и каждый элемент a[i] обновляет максимум один элемент dp[j]
    for (int i = 1; i <= n; ++i) {
        dp[i] = std::numeric_limits<int>::max();
    }
    for (int i = 0; i <= n - 1; ++i) {
        int j = bin_search(a[i], n, dp);
        if (dp[j] >= a[i] && dp[j - 1] < a[i] && a[i] < dp[j]) {
            dp[j] = a[i];
            pos[j] = i;
            prev[i] = pos[j - 1];
            length = std::max(length, j);
        }
    }
    vector<int> answer;
    int p = pos[length];
    while (p != -1) {
        answer.push_back(a[p]);
        p = prev[p];
    }
    std::reverse(answer.begin(),answer.end());
    return answer;
}

int bin_search(int a, int n, const vector<int> &dp) {
    int left = 0;
    int right = n;
    while (right - left > 1) {
        int mid = (left + right) / 2;
        if (dp[mid] >= a) {
            right = mid;
        }
        else {
            left = mid + 1;
        }
    }
    if (dp[left] >= a) right = left;
    return right;
}

int main() {
    std::ios_base::sync_with_stdio(0);
    std::cin.tie(0);
    std::cout.tie(0);
    int a;
    std::cin >> a;
    vector<int> m(a);
    for (int i = 0; i < a; ++i) {
        std::cin >> m[i];
    }
    vector <int> copy (m);
    std::sort(copy.begin(), copy.end());
    map<int,int> exchange;
    map<int,int> cur;
    for (int i = 0; i < a; ++i) {
        exchange[copy[i]] = a-i-1;
    }
    for (int i = 0; i < a; ++i) {
        cur[m[i]]++;
        m[i] = cur[m[i]] + exchange[m[i]];
    }
    vector<int> answer(findLIS(m));
    map<int,int> find_ind;
    for (int i = 0; i < a; ++i) {
        find_ind[m[i]] = i + 1;
    }
    std::cout << answer.size() << std::endl;
    for (size_t i = 0; i < answer.size(); ++i) {
        std::cout << find_ind[answer[i]] << ' ';
    }
    return 0;
}