#include <vector>
#include <iostream>
#include <type_traits>

using std::vector;

template<size_t chunkSize>
class FixedAllocator {
public:
    int8_t* returnChunk();
    void addMemory();
    void takeChunk(int8_t* ptr);
    ~FixedAllocator();
    static FixedAllocator<chunkSize>& returnAlloc() {
        static FixedAllocator<chunkSize> alloc;
        return alloc;
  }
private:
    const size_t bucketSize = 30;
    std::vector<int8_t*> buck;
    std::vector<int8_t*> free;
};

template<size_t chunkSize>
int8_t* FixedAllocator<chunkSize>::returnChunk() {
    if (free.empty()) {
        addMemory();
    }
    int8_t* tmp = free.back();
    free.pop_back();
    return tmp;
}

template<size_t chunkSize>
void FixedAllocator<chunkSize>::addMemory() {
    int8_t* bucket = new int8_t[bucketSize * chunkSize];
    buck.push_back(bucket);
    free.reserve(free.size()+bucketSize);
    for (size_t i = 0; i < bucketSize; ++i) {
        free.push_back(bucket + i * chunkSize);
    }
}

template<size_t chunkSize>
void FixedAllocator<chunkSize>::takeChunk(int8_t* ptr) {
    free.push_back(ptr);
}

template<size_t chunkSize>
FixedAllocator<chunkSize>::~FixedAllocator() {
    while (!buck.empty()) {
        int8_t* ptr = buck.back();
        buck.pop_back();
        delete[] ptr;
    }
}

template<typename T>
class FastAllocator {
public:
    using value_type = T;
    FastAllocator() = default;
    template<typename U>
    FastAllocator(const FastAllocator <U>& ) {}
    template<typename U>
    FastAllocator& operator = (const FastAllocator<U>& ) const {
        return *this;
    }
    T* allocate(size_t n);
    void deallocate(T* ptr, size_t n);
private:
    static const size_t maxFixed = 24;
};

template<typename T>
T* FastAllocator<T>::allocate(size_t n) {
    if (n * sizeof(T) > maxFixed || n > 1) {
        return reinterpret_cast<T*>(malloc(n * sizeof(T)));
    }
    else {
        return reinterpret_cast<T*>(FixedAllocator<sizeof(T)>::returnAlloc().returnChunk());
    }
}

template<typename T>
void FastAllocator<T>::deallocate(T *ptr, size_t n) {
    if (n * sizeof(T) > maxFixed || n > 1) {
        free(ptr);
    }
    else {
        FixedAllocator<sizeof(T)>::returnAlloc().takeChunk(reinterpret_cast<int8_t*>(ptr));
    }
}

template<class T, class U>
bool operator == (const FastAllocator<T>& a, const FastAllocator<U>& b) {
    return &a == &b;
}

template<class T, class U>
bool operator != (const FastAllocator<T>& a, const FastAllocator<U>& b) {
    return &a != &b;
}

template <typename T, typename Allocator = std::allocator<T>>
class List {
private:
    template <bool IsConst>
    class common_iterator;
public:
    using iterator = common_iterator <false> ;
    using const_iterator = common_iterator <true> ;
    using reverse_iterator = std::reverse_iterator <iterator> ;
    using const_reverse_iterator = std::reverse_iterator <const_iterator > ;

    explicit List(const Allocator & alloc = Allocator());

    explicit List(size_t count, const T& value, const Allocator& alloc = Allocator());

    explicit List(size_t count, const Allocator& alloc = Allocator());

    List(const List<T, Allocator>& lst);

    ~List();

    void push_front(const T& value);

    void pop_front();

    void push_back(const T& value);

    void pop_back();

    size_t size() const;

    Allocator get_allocator() const;

    List& operator = (const List< T, Allocator>& lst);

    iterator begin() const {
        return iterator(tale -> next);
    }

    iterator end() const {
        return iterator(tale);
    }

    const_iterator cbegin() const {
        return const_iterator(tale -> next);
    }

    const_iterator cend() const {
        return const_iterator(tale);
    }

    reverse_iterator rbegin() const {
        return reverse_iterator(tale);
    }

    reverse_iterator rend() const {
        return reverse_iterator(tale -> next);
    }

    const_reverse_iterator crbegin() const {
        return const_reverse_iterator(tale);
    }

    const_reverse_iterator crend() const {
        return const_reverse_iterator(tale -> next);
    }

    void insert(const const_iterator& it, const T& value) {
        Node* tmp = const_cast <Node*> (it.node);
        insert_before(tmp, value);
    }

    void erase(const const_iterator & it) {
        Node* tmp = const_cast < Node * > (it.node);
        erase(tmp);
    }

private:
    struct Node {
        Node* prev = nullptr;
        Node* next = nullptr;
        T value;
        Node(const T& val): value(val) {}
        Node() = default;
    };

    size_t sz = 0;
    Node* tale;
    void push_back();
    typename std::allocator_traits<Allocator>::template rebind_alloc<Node> NodeAlloc;
    using NodeAllocTraits = std::allocator_traits <decltype(NodeAlloc)>;

    void initTale() {
        tale = NodeAlloc.allocate(1);
        tale -> next = tale;
        tale -> prev = tale;
    }

    void erase(Node* node);

    Node* createNode(const T& value) {
        Node* newNode = NodeAlloc.allocate(1);
        NodeAllocTraits::construct(NodeAlloc, newNode, value);
        return newNode;
    }

    Node* createNode() {
        Node* newNode = NodeAlloc.allocate(1);
        NodeAllocTraits::construct(NodeAlloc, newNode);
        return newNode;
    }

    void insert_before(Node* node, const T& value);

    template <bool IsConst>
    class common_iterator {
    public:
        std::conditional_t<IsConst, const Node* , Node*> node;
        using TT = std::conditional_t < IsConst,
                const T, T > ;
        using iterator_category = std::bidirectional_iterator_tag;
        using value_type = TT;
        using reference = TT & ;
        using pointer = TT * ;
        using difference_type = std::ptrdiff_t;
        common_iterator(): node(nullptr) {}
        common_iterator(std::conditional_t<IsConst, const Node* , Node*> t): node(t) {}
        common_iterator(const common_iterator & it): node(it.node) {}
        operator common_iterator <true> () {
            return common_iterator <true> (node);
        }
        common_iterator& operator++() {
            node = node -> next;
            return *this;
        }

        common_iterator& operator--() {
            if (node != nullptr) {
                node = node -> prev;
            }
            return *this;
        }

        common_iterator operator++(int) {
            common_iterator copy( * this);
            ++( * this);
            return copy;
        }

        common_iterator operator--(int) {
            common_iterator copy( * this);
            --( * this);
            return copy;
        }

        std::conditional_t <IsConst,
                const T& , T&> operator * () const {
            return node -> value;
        }

        bool operator == (common_iterator it) {
            return node == it.node;
        }

        bool operator != (common_iterator it) {
            return node != it.node;
        }

        std::conditional_t <IsConst,
                const T* , T*> operator -> () const {
            return &(node -> value);
        }

        common_iterator& operator = (const common_iterator <false>& it) {
            node = it.node;
            return *this;
        }
    };
};

template<typename T, typename Allocator>
List<T, Allocator>::List(const Allocator &alloc): NodeAlloc(alloc) {
    initTale();
}

template<typename T, typename Allocator>
List<T, Allocator>::List(size_t count, const T &value, const Allocator &alloc): NodeAlloc(alloc) {
    initTale();
    for (size_t i = 0; i < count; ++i) {
        push_back(value);
    }
}

template<typename T, typename Allocator>
List<T, Allocator>::List(size_t count, const Allocator &alloc): NodeAlloc(alloc) {
    initTale();
    for (size_t i = 0; i < count; ++i) {
        push_back();
    }
}

template<typename T, typename Allocator>
List<T, Allocator>::List(const List<T, Allocator> &lst) {
    NodeAlloc = NodeAllocTraits::select_on_container_copy_construction(lst.NodeAlloc);
    initTale();
    Node* tmp = lst.tale -> next;
    while (tmp != lst.tale) {
        push_back(tmp -> value);
        tmp = tmp -> next;
    }
}

template<typename T, typename Allocator>
List<T, Allocator>::~List() {
    while (sz>0) {
        pop_back();
    }
    NodeAlloc.deallocate(tale, 1);
}

template<typename T, typename Allocator>
void List<T, Allocator>::push_front(const T &value) {
    insert_before(tale -> next, value);
}

template<typename T, typename Allocator>
void List<T, Allocator>::pop_front() {
    erase(tale -> next);
}

template<typename T, typename Allocator>
void List<T, Allocator>::push_back(const T &value) {
    Node* newNode = createNode(value);
    tale -> prev -> next = newNode;
    newNode -> prev = tale -> prev;
    newNode -> next = tale;
    tale -> prev = newNode;
    ++sz;
}

template<typename T, typename Allocator>
void List<T, Allocator>::push_back() {
    Node* newNode = createNode();
    tale -> prev -> next = newNode;
    newNode -> prev = tale -> prev;
    newNode -> next = tale;
    tale -> prev = newNode;
    ++sz;
}

template<typename T, typename Allocator>
void List<T, Allocator>::pop_back() {
    if (sz > 0) {
        erase(tale -> prev);
    }
}

template<typename T, typename Allocator>
size_t List<T, Allocator>::size() const {
    return sz;
}

template<typename T, typename Allocator>
Allocator List<T, Allocator>::get_allocator() const {
    return NodeAlloc;
}

template<typename T, typename Allocator>
List<T,Allocator>& List<T, Allocator>::operator=(const List<T, Allocator>& lst) {
    int size = sz;
    for (int i = 0; i < size; ++i) {
        pop_back();
    }
    if (NodeAllocTraits::propagate_on_container_copy_assignment::value && NodeAlloc != lst.NodeAlloc) {
        NodeAlloc = lst.NodeAlloc;
    }
    Node* tmp = lst.tale -> next;
    while (tmp != lst.tale) {
        push_back(tmp -> value);
        tmp = tmp -> next;
    }
    return *this;
}

template<typename T, typename Allocator>
void List<T, Allocator>::erase(List::Node *node) {
    node -> prev -> next = node -> next;
    node -> next -> prev = node -> prev;
    std::allocator_traits < decltype(NodeAlloc) > ::destroy(NodeAlloc, node);
    NodeAlloc.deallocate(node, 1);
    --sz;
}

template<typename T, typename Allocator>
void List<T, Allocator>::insert_before(List::Node *node, const T &value) {
    Node * newNode = createNode(value);
    node -> prev -> next = newNode;
    newNode -> prev = node -> prev;
    node -> prev = newNode;
    newNode -> next = node;
    ++sz;
}
