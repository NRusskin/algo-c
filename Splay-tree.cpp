/*B. Splay-дерево
ограничение по времени на тест: 3 секунды
ограничение по памяти на тест: 256 мегабайт
ввод: стандартный ввод
вывод: стандартный вывод
Реализуйте splay-дерево, которое поддерживает множество S целых чисел, в котором разрешается производить следующие операции:
add(i) — добавить в множество S число i (если он там уже есть, то множество не меняется);
sum(l,r) — вывести сумму всех элементов x из S, которые удовлетворяют неравенству l≤x≤r.
Решения, не использующие splay-деревья, будут игнорироваться.
Решения, не использующие операции split и merge, будут оцениваться в 2 балла. Решения, использующие операции split и merge, оцениваются в 3 балла.
Входные данные
Исходно множество S пусто. Первая строка входного файла содержит n — количество операций (1≤n≤300000). Следующие n строк содержат операции. Каждая операция имеет вид либо «+ i», либо «? l r». Операция «? l r» задает запрос sum(l,r).
Если операция «+ i» идет во входном файле в начале или после другой операции «+», то она задает операцию add(i). Если же она идет после запроса «?», и результат этого запроса был y, то выполняется операция add((i+y)mod109).
Во всех запросах и операциях добавления параметры лежат в интервале от 0 до 109.
Выходные данные
Для каждого запроса выведите одно число — ответ на запрос. */

#include <iostream>

struct Node {
    long long key;
    Node* left = nullptr;
    Node* right = nullptr;
    Node* parent = nullptr;
    long long sum = 0;
    Node(long long k, Node* l, Node* r): key(k), left(l), right(r), sum(k) {}
    Node(long long k): key(k) {}
    ~Node() {
        delete left;
        delete right;
    }
};

class Splay_tree {
public:
    ~Splay_tree() {
        delete root;
    }
    void insert(long long key);
    void erase(long long key);
    long long sum(long long l, long long r);
private:
    Node* root = nullptr;
    long long return_sum(Node* node);
    void count_sum(Node* node);
    void set_parent(Node* child, Node* parent);
    void keep_parent(Node* node);
    Node* rotate_left(Node* node);
    Node* rotate_right(Node* node);
    std::pair <Node*, Node*> split(Node* node, long long key);
    Node* merge(Node* left, Node* right);
    bool find(Node* root, long long key);
    Node* splay(Node* node);
    Node* search(Node* node, long long key);
};

long long Splay_tree::return_sum(Node* node) {
    if (!node) return 0;
    return node -> sum;
}

void Splay_tree::set_parent(Node* child, Node* parent) {
    if (child) child -> parent = parent;
    return;
}

void Splay_tree::count_sum(Node* node) {
    node -> sum = return_sum(node -> left) + return_sum(node -> right) + node -> key;
}

void Splay_tree::keep_parent(Node* node) {
    if (!node) return;
    set_parent(node -> left, node);
    set_parent(node -> right, node);
    return;
}

Node* Splay_tree::rotate_left(Node* node) {
    Node* gparent = node -> parent;
    if (gparent) {
        if (gparent -> left == node)
            gparent -> left = node -> right;
        else
            gparent -> right = node -> right;
    }
    Node* tmp = node -> right;
    node -> right = tmp -> left;
    tmp -> left = node;
    tmp -> parent = node -> parent;
    keep_parent(node);
    keep_parent(tmp);
    count_sum(node);
    count_sum(tmp);
    return tmp;
}

Node* Splay_tree::rotate_right(Node* node) {
    Node* gparent = node -> parent;
    if (gparent) {
        if (gparent -> left == node)
            gparent -> left = node -> left;
        else
            gparent -> right = node -> left;
    }
    Node* tmp = node -> left;
    node -> left = tmp -> right;
    tmp -> right = node;
    tmp -> parent = node -> parent;
    keep_parent(node);
    keep_parent(tmp);
    count_sum(node);
    count_sum(tmp);
    return tmp;
}

bool Splay_tree::find(Node* root, long long key) {
    if (!root) return false;
    if (root -> key < key) return find(root -> right, key);
    if (root -> key > key) return find(root -> left, key);
    return true;
}

Node* Splay_tree::splay(Node* node) {
    if (!node -> parent) return node;
    Node* parent = node -> parent;
    Node* gparent = parent -> parent;
    if (!gparent) {
        if (node == parent -> left) node = rotate_right(parent);
        else node = rotate_left(parent);
        return node;
    } 
    else {
        bool zigzig = ((gparent -> left == parent) == (parent -> left == node));
        if (zigzig) {
            if (gparent -> left == parent) {
                parent = rotate_right(gparent);
                node = rotate_right(parent);
            } 
            else {
                parent = rotate_left(gparent);
                node = rotate_left(parent);
            }
        } 
        else {
            if (gparent -> left == parent) {
                node = rotate_left(parent);
                node = rotate_right(gparent);
            } 
            else {
                node = rotate_right(parent);
                node = rotate_left(gparent);
            }
        }
        return splay(node);
    }
}

Node* Splay_tree::search(Node* node, long long key) {
    if (!node) return nullptr;
    if (key == node -> key) return splay(node);
    if (key < node -> key && node -> left) return search(node -> left, key);
    if (key > node -> key && node -> right) return search(node -> right, key);
    return splay(node);
}

std::pair <Node*, Node*> Splay_tree::split(Node* node, long long key) {
    if (!node) return {nullptr,nullptr};
    node = search(node, key);
    if (node -> key <= key) {
        Node* right = node -> right;
        node -> right = nullptr;
        set_parent(right, nullptr);
        count_sum(node);
        return {node,right};
    } 
    else {
        Node* left = node -> left;
        node -> left = nullptr;
        set_parent(left, nullptr);
        count_sum(node);
        return {left,node};
    }
}

void Splay_tree::insert(long long key) {
    if (find(root, key)) return;
    std::pair <Node*, Node*> parts = split(root, key);
    root = new Node(key, parts.first, parts.second);
    keep_parent(root);
    count_sum(root);
    return;
}

Node* Splay_tree::merge(Node* left, Node* right) {
    if (!right) return left;
    if (!left) return right;
    right = search(right, left -> key);
    right -> left = left;
    left -> parent = right;
    count_sum(right);
    return right;
}

void Splay_tree::erase(long long key) {
    Node* extracted = search(root, key);
    set_parent(extracted -> left, nullptr);
    set_parent(extracted -> right, nullptr);
    root = merge(extracted -> left, extracted -> right);
    return;
}

long long Splay_tree::sum(long long l, long long r) {
    std::pair <Node*, Node*> parts = split(root, l - 1);
    std::pair <Node*, Node*> next_parts = split(parts.second, r);
    long long s = return_sum(next_parts.first);
    parts.second = merge(next_parts.first, next_parts.second);
    root = merge(parts.first, parts.second);
    return s;
}

int main() {
    std::ios::sync_with_stdio(false);
    std::cin.tie(0);
    char c;
    bool indicator = false;
    Splay_tree tree;
    long long sum_segment = 0;
    long long m, l, r;
    long long q;
    std::cin >> q;
    for (int i = 0; i < q; ++i) {
        std::cin >> c;
        if (c == '+') {
            std::cin >> m;
            if (indicator) {
                m = (m + sum_segment)%1000000000;
                indicator = false;
            }
            tree.insert(m);
        } 
        else {
            indicator = true;
            std::cin >> l >> r;
            sum_segment = tree.sum(l, r);
            std::cout << sum_segment << std::endl;
        }
    }
    return 0;
}