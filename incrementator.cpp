/*A. Incrementator
ограничение по времени на тест: 2 секунды
ограничение по памяти на тест: 256 мегабайт
ввод: стандартный ввод
вывод: стандартный вывод
Ваша задача — написать программу, моделирующую простое устройство, которое умеет прибавлять целые значения к целочисленным переменным.
Входные данные
Входной файл состоит из одной или нескольких строк, описывающих операции. Строка состоит из названия переменной и числа, которое к этой переменной надо добавить. Все числа не превосходят 100 по абсолютной величине. Изначально все переменные равны нулю. Названия переменных состоят из не более чем 100000 маленьких латинских букв. Размер входного файла не превосходит 2 мегабайта.
Выходные данные
Для каждой операции выведите на отдельной строке значение соответствующей переменной после выполнения операции.*/


#include <iostream>
#include <string>
#include <vector>
#include <list>
 
using std::vector;
using std::list;
using std::string;
 
const int p = 997;
const int mod = 558343;
 
struct Pair {
    const string key;
    int value;
    Pair (const string& k, int v): key(k), value(v) {}
};
 
class HashTable {
public:
    HashTable() {
        data.resize(mod);
    }
    void del (const string& k);
    void put (const string& k, int v);
    int get (const string& k);
private:
     vector<list<Pair>> data;
     int count_hash (const string& s);
     void delHash (int hash, const string& k);
};
 
void HashTable::delHash(int hash, const string& k) {
    for (auto it = data[hash].begin(); it != data[hash].end(); ) {
        if (it->key == k) {
            it = data[hash].erase(it);
            return;
        } 
        else ++it;
    }
    return;
}
 
void HashTable::del(const string& k) {
    int hash = count_hash(k);
    delHash(hash, k);
}
 
void HashTable::put (const string& k, int v) {
    int hash = count_hash(k);
    int delta = get(k);
    delHash(hash, k);
    data[hash].push_back(Pair(k,v+delta));
    return;
}
 
int HashTable::get (const string& k) {
    int hash = count_hash(k);
    for (auto p:data[hash]) {
        if (p.key == k) {
            return p.value;
        }
    }
    return 0;
}
 
int HashTable::count_hash (const string& k) {
    int hash = 0; 
    int cur = 1;
    for (size_t i = 0; i < k.size();i++) {
        hash += int(k[i])*cur; 
        cur *= p; 
        cur %= mod; 
        hash %= mod;
    }
    return hash;
}
 
int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);
    std::cout.tie(nullptr);
    string s;
    int x;
    HashTable table;
    while (std::cin>>s) {
        std::cin>>x;
        table.put(s,x);
        std::cout<<table.get(s)<<std::endl;
    }
    return 0;
}