#include <iostream>
#include <vector>
#include <cmath>

const double EPS = 1e-7;

bool equ(double a, double b) {
    return std::abs(a - b) < EPS;
}

double det(double a, double b, double c, double d) {
    return a * d - b * c;
}

struct Point {
    double x = 0;
    double y = 0;
    Point() {}
    Point(double first, double second): x(first), y(second) {}
};

bool operator == (const Point & a, const Point & b) {
    return (equ(a.x, b.x) && equ(a.y, b.y));
}

bool operator != (const Point & a, const Point & b) {
    return !(a == b);
}

bool forward(Point a, Point b, Point c) {
    return a.x * (b.y - c.y) + b.x * (c.y - a.y) + c.x * (a.y - b.y) < 0;
}

bool backward(Point a, Point b, Point c) {
    return a.x * (b.y - c.y) + b.x * (c.y - a.y) + c.x * (a.y - b.y) > 0;
}

double dist(Point first, Point second) {
    return std::sqrt((first.x - second.x) * (first.x - second.x) + (first.y - second.y) * (first.y - second.y));
}

class Line {
public:
    double a = 1;
    double b = 1;
    double c = 0;
    Line() {}
    Line(Point first, Point second): a(first.y - second.y), b(second.x - first.x), c(first.x * second.y - first.y * second.x) {}
    Line(double k, double b): a(k), b(-1), c(b) {}
    Line(Point point, double k): a(k), b(-1), c(point.y - point.x * k) {}
    Line(double a, double b, double c): a(a), b(b), c(c) {}
};

bool operator == (const Line & first, const Line & second) {
    return equ(first.a * second.b, second.a * first.b) && equ(first.a * second.c, second.a * first.c);
}

bool operator != (const Line & first, const Line & second) {
    return !(first == second);
}

Point intersect(Line m, Line n) {
    double zn = det(m.a, m.b, n.a, n.b);
    if (std::abs(zn) < EPS)
        return Point(-m.c / m.a, 0);
    return Point(-det(m.c, m.b, n.c, n.b) / zn, -det(m.a, m.c, n.a, n.c) / zn);
}

Point proection(Point seq, Line l) {
    if (l.a == 0) return Point(seq.x, -l.c / l.b);
    else if (l.b == 0) return Point(-l.c / l.a, seq.y);
    Line perp(seq, -1 / (l.a / (-l.b)));
    return intersect(perp, l);
}

Point mirror(Point seq, Line l) {
    Point pr = proection(seq, l);
    return Point(pr.x - (seq.x - pr.x), pr.y - (seq.y - pr.y));
}

Point rotation(Point w, Point c, double angle) {
    return Point(c.x + (w.x - c.x) * cos(angle) - (w.y - c.y) * sin(angle), c.y + (w.x - c.x) * sin(angle) + (w.y - c.y) * cos(angle));
}

class Shape {
public:
    virtual double perimeter() = 0;
    virtual double area() = 0;
    virtual bool isCongruentTo(const Shape & another) = 0;
    virtual bool isSimilarTo(const Shape & another) = 0;
    virtual bool containsPoint(Point point) = 0;
    virtual ~Shape() {}
    virtual void scale(Point center, double coefficient) = 0;
    virtual void reflex(Point cen)=0;
    virtual void rotate(Point center, double angle) = 0;
    virtual void reflex(Line axis) = 0;
    virtual bool operator==(const Shape& another)const =0;
    virtual bool operator!=(const Shape& another)const =0;
};

class Polygon: public Shape {
public: 
    std::vector < Point > vertices;
    Polygon() = default;
    
    Polygon(const std::vector < Point > & vert) {
        vertices = vert;
    }
    
    Polygon(const std::initializer_list < Point > & list) {
        for (auto & element: list) {
            vertices.push_back(element);
        }
    }
    
    size_t verticesCount() {
        return vertices.size();
    }
    
    std::vector < Point > getVertices() {
        return vertices;
    }
    
    double perimeter();
    
    void reflex(Line axis) {
        for (size_t i = 0; i < vertices.size(); ++i)
            vertices[i] = mirror(vertices[i], axis);
    }
    
    void scale(Point center, double coefficient);
    
    void reflex(Point cen) {
        scale(cen, 1);
    }
    void rotate(Point center, double angle);
    
    double area();

    bool containsPoint(Point point);

    bool isConvex();

    bool isSimilarTo(const Shape & another);

    bool isCongruentTo(const Shape & another);
    bool operator == (const Shape& another) const;
    bool operator != (const Shape & another) const{
        return !(*this == another);
    }
};

bool Polygon::operator == (const Shape& another) const{
    const Polygon* thx= dynamic_cast<const Polygon*>(&another);
    if (!thx) return false;
    for (size_t i = 0; i < vertices.size(); ++i) {
        bool indicator = true;
        for (size_t j = 0; j < vertices.size(); ++j) {
            if (vertices[(i + j) % vertices.size()] != thx->vertices[j]) {
                indicator = false;
                break;
            }
        }
        if (indicator) return true;
    }
    for (size_t i = 0; i < vertices.size(); ++i) {
        bool indicator = true;
        for (size_t j = 0; j < vertices.size(); ++j) {
            if (vertices[(i - j + vertices.size()) % vertices.size()] != thx->vertices[j]) {
                indicator = false;
                break;
            }
        }
        if (indicator) return true;
    }
    return false;
}

double Polygon::perimeter() {
    double p=0;
    for (size_t i=0; i<vertices.size()-1; ++i) 
    p+=dist(vertices[i],vertices[i+1]);
    p+=dist(vertices[0], vertices[vertices.size()-1]);
    return p;
}

void Polygon::scale(Point center, double coefficient) {
    for (size_t i = 0; i < vertices.size(); ++i) {
        double dx = center.x - vertices[i].x;
        double dy = center.y - vertices[i].y;
        vertices[i].x = center.x + dx * coefficient;
        vertices[i].y = center.y + dy * coefficient;
    }
}

void Polygon::rotate(Point center, double angle) {
    angle /= 180 / M_PI;
    for (size_t i = 0; i < vertices.size(); ++i) {
        Point tmp(vertices[i].x - center.x, vertices[i].y - center.y);
        tmp = rotation(tmp, center, angle);
        vertices[i].x = tmp.x + center.x;
        vertices[i].y = tmp.y + center.y;
    }
}

double Polygon::area() {
    double answer = 0;
    for (size_t i = 0; i < vertices.size(); ++i) {
        answer += vertices[i].x * vertices[(i + 1) % vertices.size()].y;
    }
    for (size_t i = 0; i < vertices.size(); ++i) {
        answer -= vertices[i].x * vertices[(i - 1 + vertices.size()) % vertices.size()].y;
    }
    answer /= 2;
    return std::abs(answer);
}

bool Polygon::containsPoint(Point point) {
    bool ans = false;
    size_t size = vertices.size();
    for (size_t i = 0, j = size - 1; i < size; j = i++) {
        if ((((vertices[i].y <= point.y) && (point.y < vertices[j].y)) || ((vertices[j].y <= point.y) && (point.y < vertices[i].y))) &&
            (((vertices[j].y - vertices[i].y) != 0) && (point.x > ((vertices[j].x - vertices[i].x) * (point.y - vertices[i].y) / (vertices[j].y - vertices[i].y) + vertices[i].x))))
            ans = !ans;
    }
    return ans;
}

bool Polygon::isConvex() {
    size_t size = vertices.size();
    bool indicator;
    Point A, B, C;
    if (forward(vertices[0], vertices[1], vertices[2])) indicator = true;
    for (size_t i = 0; i < size; ++i) {
        A = vertices[i];
        B = vertices[(i + 1) % size];
        C = vertices[(i + 2) % size];
        if (indicator && backward(A, B, C)) return false;
        if (!indicator && forward(A, B, C)) return false;
    }
    return true;
}

bool Polygon::isSimilarTo(const Shape & another) {
    const Polygon* thx= dynamic_cast<const Polygon*>(&another);
    if (!thx) return false;
    size_t size = vertices.size();
    for (size_t i = 0; i < size; ++i) {
        bool indicator = true;
        for (size_t j = 0; j < size; ++j) {
            for (size_t k = 0; k < size; ++k)
                if (!equ(dist(vertices[j], vertices[k]) * dist(thx->vertices[i], thx->vertices[(i + 1) % size]), dist(vertices[0], vertices[1]) * dist(thx->vertices[(j + i) % size], thx->vertices[(k + i) % size]))) {
                    indicator = false;
                    break;
                }
            if (!indicator) break;
        }
        if (indicator) return true;
    }
    for (size_t i = 0; i < size; ++i) {
        bool indicator = true;
        for (size_t j = 0; j < size; ++j) {
            for (size_t k = 0; k < size; ++k)
                if (!equ(dist(vertices[j], vertices[k]) * dist(thx->vertices[i], thx->vertices[(i - 1 + size) % size]),
                        dist(vertices[0], vertices[1]) * dist(thx->vertices[(size - j + i) % size], thx->vertices[(size - k + i) % size]))) {
                    indicator = false;
                    break;
                }
            if (!indicator) break;
        }
        if (indicator) return true;
    }

    return false;
}

bool Polygon::isCongruentTo(const Shape & another) {
    const Polygon* thx= dynamic_cast<const Polygon*>(&another);
    if (!thx) return false;
    size_t size = vertices.size();
    if (size != thx->vertices.size()) return false;
    for (size_t i = 0; i < size; ++i) {
        bool indicator = true;
        for (size_t j = 0; j < size; ++j) {
            for (size_t k = 0; k < size; ++k)
                if (!equ(dist(vertices[j], vertices[k]),
                        dist(thx->vertices[(j + i) % size], thx->vertices[(k + i) % size]))) {
                    indicator = false;
                    break;
                }
            if (!indicator) break;
        }
        if (indicator) return true;
    }
    for (size_t i = 0; i < size; ++i) {
        bool indicator = true;
        for (size_t j = 0; j < size; ++j) {
            for (size_t k = 0; k < size; ++k)
                if (!equ(dist(vertices[j], vertices[k]),
                        dist(thx->vertices[(size - j + i) % size], thx->vertices[(size - k + i) % size]))) {
                    indicator = false;
                    break;
                }
            if (!indicator) break;
        }
        if (indicator) return true;
    }
    return false;
}

class Ellipse: public Shape {
public: 
    std::pair < Point, Point > f;
    double a;
    
    Ellipse() = default;
    
    Ellipse(Point first, Point second, double x) {
        f.first = first;
        f.second = second;
        a = x / 2;
    }
    
    std::pair < Point, Point > focuses() {
        return f;
    }
    
    Point center() {
        return Point((f.first.x + f.second.x) / 2, (f.first.y + f.second.y) / 2);
    }
    
    std::pair < Line, Line > directrices();

    double eccentricity() {
        return dist(f.first, f.second) / (2 * a);
    }
    
    virtual double perimeter() {
        double c = dist(f.first, f.second) / 2;
        double b = std::sqrt(a * a - c * c);
        double p = M_PI * (3 * (a + b) - std::sqrt((3 * a + b) * (a + 3 * b)));
        return p;
    }
    
    void reflex(Line axis) {
        f.first = mirror(f.first, axis);
        f.second = mirror(f.second, axis);
    }
    
    void scale(Point center, double coefficient);
    
    void reflex(Point cen) {
        scale(cen, 1);
    }
    
    double area() {
        double c = dist(f.first, f.second) / 2;
        double b = std::sqrt(a * a - c * c);
        return M_PI * a * b;
    }
    
    void rotate(Point center, double angle);

    bool containsPoint(Point point) {
        double distantion = dist(f.first, point) + dist(f.second, point);
        if (distantion < a * 2 + EPS) return true;
        else return false;
    }
    
    bool isCongruentTo(const Shape & another) {
        const Ellipse*  thx = dynamic_cast<const Ellipse*>(&another);
        if (!thx) return false;
        if (!equ(dist(f.first, f.second), dist(thx->f.first, thx->f.second))) return false;
        if (!equ(a, thx->a)) return false;
        return true;
    }

    bool isSimilarTo(const Shape & another) {
        const Ellipse*  thx = dynamic_cast<const Ellipse*>(&another);
        if (!thx) return false;
        if (!equ(a * dist(thx->f.first, thx->f.second) / thx->a, dist(f.first, f.second))) return false;
        else return true;
    }
    bool operator ==(const Shape& another)const;
    bool operator != (const Shape & another) const{
        return !(*this == another);
    }
};

bool Ellipse::operator == (const Shape& another) const {
    const Ellipse*  thx = dynamic_cast<const Ellipse*>(&another);
    if (!thx) return false;
    if (f.first == thx->f.first && f.second == thx->f.second && equ(a, thx->a)) return true;
    if (f.first == thx->f.second && f.second == thx->f.first && equ(a, thx->a)) return true;
    return false;
}

std::pair < Line, Line > Ellipse::directrices() {
    double c = dist(center(), f.first);
    Point l1(center().x + (f.first.x - center().x) * a * a / (c * c), center().y + (f.first.y - center().y) * a * a / (c * c));
    Point l2(center().x + (f.second.x - center().x) * a * a / (c * c), center().y + (f.second.y - center().y) * a * a / (c * c));
    Line L1(l1, rotation(center(), l1, 3 / 2 * M_PI));
    Line L2(l2, rotation(center(), l2, 1 / 2 * M_PI));
    return {L1,L2};
}
    
void Ellipse::rotate(Point center, double angle) {
    angle /= 180 / M_PI;
    Point tmp(f.first.x - center.x, f.first.y - center.y);
    tmp = rotation(tmp, center, angle);
    f.first.x = tmp.x + center.x;
    f.first.y = tmp.y + center.y;
    tmp = Point(f.second.x - center.x, f.second.y - center.y);
    tmp = rotation(tmp, center, angle);
    f.second.x = center.x + tmp.x;
    f.second.y = center.y + tmp.y;
}
    
void Ellipse::scale(Point center, double coefficient) {
    a *= coefficient;
    f.first.x = center.x + (center.x - f.first.x) * coefficient;
    f.first.y = center.y + (center.y - f.first.y) * coefficient;
    f.second.x = center.x + (center.x - f.second.x) * coefficient;
    f.second.y = center.y + (center.y - f.second.y) * coefficient;
}
    
class Circle: public Ellipse {
public: 
    Circle(Point m, double r) {
        f.first = f.second = m;
        a = r * 2;
    }
    
    double radius() {
        return a / 2;
    }
};

class Rectangle: public Polygon {
public: 
    Point center() {
        return (Point((vertices[0].x + vertices[2].x) / 2, (vertices[0].y + vertices[2].y) / 2));
    }

    std::pair < Line, Line > diagonals() {
        return {
            Line(vertices[0], vertices[2]),
            Line(vertices[1], vertices[3])
        };
    }
    
    Rectangle(Point first, Point second, double p);
    
    Rectangle(const std::initializer_list < Point > & list) {
        for (auto & element: list) {
            vertices.push_back(element);
        }
    }
};

Rectangle::Rectangle(Point first, Point second, double p) {
    vertices.push_back(first);
    double c = dist(first, second);
    double b = c / std::sqrt(p * p + 1);
    double b1 = b * p;
    b = std::min(b1, b);
    Point tmp(first.x + (second.x - first.x) * b / c, first.y + (second.y - first.y) * b / c);
    tmp = rotation(tmp, first, acos(b / c));
    vertices.push_back(tmp);
    vertices.push_back(second);
    tmp = Point(first.x + second.x - tmp.x, first.y + second.y - tmp.y);
    vertices.push_back(tmp);
}
    
class Square: public Rectangle {
public: 
    Square(Point first, Point second): Rectangle(first, second, 1) {}

    Circle circumscribedCircle() {
        return Circle(center(), dist(vertices[1], center()));
    }
    
    Circle inscribedCircle() {
        return Circle(center(), dist(vertices[1], vertices[2]) / 2);
    }
};

class Triangle: public Polygon {
public:
    Triangle(Point A, Point B, Point C): Polygon({A,B,C}) {}
    
    Circle circumscribedCircle();
    
    Circle inscribedCircle();
    
    Point centroid() {
        Point m((vertices[0].x + vertices[1].x) / 2, (vertices[0].y + vertices[1].y) / 2);
        return Point(vertices[2].x + (m.x - vertices[2].x) * 2 / 3, vertices[2].y + (m.y - vertices[2].y) * 2 / 3);
    }
   
    Point orthocenter() {
        Line l1(vertices[0], proection(vertices[0], Line(vertices[1], vertices[2])));
        Line l2(vertices[1], proection(vertices[1], Line(vertices[0], vertices[2])));
        return intersect(l1, l2);
    }
    
    Line EulerLine() {
        if (orthocenter() == centroid()) return Line(centroid(), circumscribedCircle().a / 2);
        else return Line(orthocenter(), centroid());
    }

    Circle ninePointsCircle();
};

Circle Triangle::circumscribedCircle() {
    double A = dist(vertices[1], vertices[2]);
    double B = dist(vertices[2], vertices[0]);
    double C = dist(vertices[1], vertices[0]);
    double rad = A * B * C / std::sqrt((A + B + C) * (B + C - A) * (C + A - B) * (A + B - C));
    double D = 2 * (vertices[0].x * (vertices[1].y - vertices[2].y) + vertices[1].x * (vertices[2].y - vertices[0].y) + vertices[2].x * (vertices[0].y - vertices[1].y));
    double U1 = ((vertices[0].x * vertices[0].x + vertices[0].y * vertices[0].y) * (vertices[1].y - vertices[2].y) + (vertices[1].x * vertices[1].x + vertices[1].y * vertices[1].y) * (vertices[2].y - vertices[0].y) + (vertices[2].x * vertices[2].x + vertices[2].y * vertices[2].y) * (vertices[0].y - vertices[1].y)) / D;
    double U2 = ((vertices[0].x * vertices[0].x + vertices[0].y * vertices[0].y) * (vertices[2].x - vertices[1].x) + (vertices[1].x * vertices[1].x + vertices[1].y * vertices[1].y) * (vertices[0].x - vertices[2].x) + (vertices[2].x * vertices[2].x + vertices[2].y * vertices[2].y) * (vertices[1].x - vertices[0].x)) / D;
    return Circle(Point(U1, U2), rad);
}

Circle Triangle::inscribedCircle() {
    double A = dist(vertices[1], vertices[2]);
    double B = dist(vertices[2], vertices[0]);
    double C = dist(vertices[1], vertices[0]);
    Point cent((A * vertices[0].x + B * vertices[1].x + C * vertices[2].x) / (A + B + C), (A * vertices[0].y + B * vertices[1].y + C * vertices[2].y) / (A + B + C));
    double rad = 0.5 * std::sqrt((B + C - A) * (C + A - B) * (A + B - C) / (A + B + C));
    return Circle(cent, rad);
}

Circle Triangle::ninePointsCircle() {
    Point A = proection(vertices[0], Line(vertices[1], vertices[2]));
    Point B = proection(vertices[1], Line(vertices[0], vertices[2]));
    Point C = proection(vertices[2], Line(vertices[1], vertices[0]));
    std::vector < Point > ut = {A,B,C};
    Triangle T({A,B,C});
    return T.circumscribedCircle();
}
