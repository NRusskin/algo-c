#include <iostream>
#include <iterator>
#include <vector>
#include <cassert>

template <typename T>
class Deque {
public:
    Deque();

    explicit Deque(int s);

    Deque(const Deque<T>& deq);

    Deque(int s, const T& temp);

    Deque& operator = (const Deque < T > & deq);

    T & operator[](size_t f) {
        return data[mass_ind(f)][elem_ind(f)];
    }
    const T & operator[](size_t f) const {
        return data[mass_ind(f)][elem_ind(f)];
    }

    size_t size() const {
        return sz;
    }

    T & at(size_t f) {
        if (f >= sz || f < 0) { 
            throw std::out_of_range("...");
        }
        else {
            return data[mass_ind(f)][elem_ind(f)];
        }
    }

    const T & at(size_t f) const {
        if (f >= sz || f < 0) {
            throw std::out_of_range("...");
        }
        else {
            return data[mass_ind(f)][elem_ind(f)];
        }
    }

    void push_back(const T & value);

    void push_front(const T& value);

    void pop_back();

    void pop_front ();

    ~Deque();

    template <bool IsConst>
    class common_iterator {
    public:
        common_iterator(std::conditional_t<IsConst, const std::vector<T*>*, std::vector<T*>*> t, std::conditional_t<IsConst, const T*, T*> e, int a, int n): table(t), elem(e), arr(a), num(n) {}

        common_iterator& operator++() {
            if (num == mod -1 ) {
                num = 0;
                ++arr;
            }
            else ++num;
            elem = &(*table)[arr][num];
            return *this;
        }
        
        common_iterator& operator--() {
            if (num == 0) {
                num = mod-1;
                --arr;
            }
            else --num;
            elem = &(*table)[arr][num];
            return *this;
        }

        common_iterator operator++ (int) {
            common_iterator copy(* this);
            ++(*this);
            return copy;
        }

        common_iterator operator-- (int) {
            common_iterator copy(* this);
            --(*this);
            return copy;
        }

        std::conditional_t<IsConst, const T&, T&> operator * () {
            return *elem;
        }
        
        bool operator < (common_iterator it) {
            return it.arr*mod+it.num > arr*mod+num;
        }
        
        bool operator > (common_iterator it) {
            return it<*this;
        }
        
        bool operator == (common_iterator it) {
            return it.arr*mod+it.num == arr*mod+num;
        }
        
        bool operator <= (common_iterator it) {
            return !(*this > it);
        }
        
        bool operator >= (common_iterator it) {
            return !(*this < it);
        }
        
        bool operator != (common_iterator it) {
            return !(*this ==it);
        }
        
        common_iterator& operator += (int t) {
            arr=(arr*mod+num+t)/mod;
            num =(arr*mod+num+t)%mod;
            elem = &((*table)[arr][num]);
            return *this;
        }

        common_iterator& operator -= (int t) {
            *this+= (-t);
            return *this;
        }
        common_iterator operator - (int t) {
            *this-=t;
            return *this;
        }
        common_iterator operator + (int t) {
            *this+=t;
            return *this;
        }
        int operator - (common_iterator it) {
            return (arr*mod+num-it.arr*mod-it.num);
        }
        std::conditional_t<IsConst, const T*, T*> operator -> () {
            return elem;
        }

    private:
        std::conditional_t<IsConst, const std::vector<T*>*, std::vector<T*>*> table;
        std::conditional_t<IsConst, const T*, T*> elem;
        int arr;
        int num;
    };

    using iterator = common_iterator<false>;
    using const_iterator = common_iterator<true>;
    using reverse_iterator = std::reverse_iterator<iterator>;
    using const_reverse_iterator = std::reverse_iterator<const_iterator>;

    iterator begin() {
        return iterator(&data, &data[ptr.arr_front][ptr.num_front], ptr.arr_front, ptr.num_front);
    }
    iterator end() {
        return iterator(&data, &data[ptr.arr_back][ptr.num_back], ptr.arr_back, ptr.num_back);
    }
    const_iterator cbegin() {
        return const_iterator(&data, &data[ptr.arr_front][ptr.num_front], ptr.arr_front, ptr.num_front);
    }
    const_iterator cend() {
        return const_iterator(&data, &data[ptr.arr_back][ptr.num_back], ptr.arr_back, ptr.num_back);
    }
    reverse_iterator rbegin() {
        return reverse_iterator(&data, &data[ptr.arr_back][ptr.num_back], ptr.arr_back, ptr.num_back);
    }
    reverse_iterator rend() {
        return reverse_iterator(&data, &data[ptr.arr_front][ptr.num_front], ptr.arr_front, ptr.num_front);
    }
    const_reverse_iterator crbegin() {
        return const_reverse_iterator(&data, &data[ptr.arr_back][ptr.num_back], ptr.arr_back, ptr.num_back);
    }
    const_reverse_iterator crend() {
        return const_reverse_iterator(&data, &data[ptr.arr_front][ptr.num_front], ptr.arr_front, ptr.num_front);
    }
    const_iterator begin() const {
        return const_iterator(&data, &data[ptr.arr_front][ptr.num_front], ptr.arr_front, ptr.num_front);
    }
    const_iterator end() const {
        return const_iterator(&data, &data[ptr.arr_back][ptr.num_back], ptr.arr_back, ptr.num_back);
    }
    const_iterator cbegin() const {
        return const_iterator(&data, &data[ptr.arr_front][ptr.num_front], ptr.arr_front, ptr.num_front);
    }
    const_iterator cend() const{
        return const_iterator(&data, &data[ptr.arr_back][ptr.num_back], ptr.arr_back, ptr.num_back);
    }
    const_reverse_iterator rbegin() const{
        return const_reverse_iterator(&data, &data[ptr.arr_back][ptr.num_back], ptr.arr_back, ptr.num_back);
    }
    const_reverse_iterator rend() const{
        return const_reverse_iterator(&data, &data[ptr.arr_front][ptr.num_front], ptr.arr_front, ptr.num_front);
    }
    const_reverse_iterator crbegin() const{
        return const_reverse_iterator(&data, &data[ptr.arr_back][ptr.num_back], ptr.arr_back, ptr.num_back);
    }
    const_reverse_iterator crend() const{
        return const_reverse_iterator(&data, &data[ptr.arr_front][ptr.num_front], ptr.arr_front, ptr.num_front);
    }
    void insert (iterator it, const T& value);
    void erase (iterator it);

private:
    struct Pointer {
        size_t arr_front;
        size_t arr_back;
        size_t num_front;
        size_t num_back;
        void update(size_t a, size_t b, size_t c, size_t d) {
            arr_front = a;
            arr_back = b;
            num_front = c;
            num_back = d;
        }
    };
    size_t capacity;
    size_t sz;
    std::vector <T*> data;
    Pointer ptr;
    static const int mod = 100;

    void swap(Deque <T>& first) {
        std::swap(first.data, data);
        std::swap(first.sz, sz);
        std::swap(first.ptr,ptr);
        std::swap(first.capacity,capacity);
    }

    void reallocate();
    int mass_ind(int f) const {
        return (ptr.arr_front * mod + ptr.num_front + f) / mod;
    }
    int elem_ind(int f) const {
        return (ptr.arr_front * mod + ptr.num_front + f) % mod;
    }
};

template<typename T>
Deque<T>::Deque(): capacity(3), sz(0), ptr() {
    data.resize(3);
    data[0] = reinterpret_cast < T * > (new int8_t[mod * sizeof(T)]);
    data[1] = reinterpret_cast < T * > (new int8_t[mod * sizeof(T)]);
    data[2] = reinterpret_cast < T * > (new int8_t[mod * sizeof(T)]);
    ptr.update(1, 1, mod / 2, mod / 2);
}

template<typename T>
Deque<T>::Deque(int s): capacity(s / mod + 2), sz(s), ptr(){
    size_t dr = s / mod;
    data.resize(dr + 2);
    size_t i = 0;
    int j = 0;
    int k = 0;
    int p = 0;
    try {
        for (; i < data.size(); ++i) {
            data[i] = reinterpret_cast < T * > (new int8_t[mod * sizeof(T)]);
        }
        for (; j <= s / mod; ++j) {
            for (; k < mod; ++k)
                new(data[j] + k) T();
        }
        for (; p < s % mod; ++p) {
            new(data[dr + 1] + p) T();
        }
    }
    catch (...) {
        for ( int g = 0; g < p; ++g) {
            (data[dr + 1] + g) -> ~T();
        }
        for (int g=0; g < j; ++g) {
            for (int h=0; h<mod; ++h)
                (data[g] + h)->~ T();
        }
        for ( int h=0; h<k; ++h) {
            (data[j]+h)->~T();
        }
        for ( size_t g=0; g < i; ++g) {
            delete[] reinterpret_cast < int8_t* > (data[g]);
        }
        throw;
    }
    ptr.update(1, dr + 1, 0, s % mod);
}

template<typename T>
Deque<T>::Deque(const Deque<T> &deq):  capacity(deq.capacity), sz(deq.sz),ptr(deq.ptr) {
    data.resize(capacity);
    size_t i=0;
    size_t point = ptr.num_front;
    size_t g =point;
    size_t p =ptr.arr_front+1;
    int j = point;
    size_t f = 0;
    int s = 0;
    try {
        for (; i < data.size(); ++i) {
            data[i] = reinterpret_cast < T * > (new int8_t[mod * sizeof(T)]);
        }
        if (ptr.arr_front == ptr.arr_back) {
            for (; g < ptr.num_back; ++g) {
                new(data[ptr.arr_front] + g) T(*(deq.data[ptr.arr_front] + g));
            }
        } else {
            for (; j<mod; ++j) {
                new(data[ptr.arr_front]+ j) T(*(deq.data[ptr.arr_front] + j));
            }
            for (; p < ptr.arr_back; ++p) {
                for (; s < mod; ++s)
                    new(data[p]+ s) T(*(deq.data[p] + s));
            }
            point = ptr.arr_back;
            for (; f < ptr.num_back; ++f) {
                new(data[point] + f) T(*(deq.data[point] + f));
            }
        }
    }
    catch(...) {
        if (ptr.arr_front == ptr.arr_back) {
            for ( size_t h; h < g; ++h) {
                (data[ptr.arr_front] + h) ->~T();
            }
        } else {
            for (int h=ptr.num_front; h<j; ++h) {
                (data[ptr.arr_front]+ h)->~ T();
            }
            for ( size_t h = ptr.arr_front+1; h<p; ++h) {
                for (int d=0; d<s; ++d)
                    (data[h]+ d)->~T();
            }
            point = ptr.arr_back;
            for ( size_t h=0; h<f; ++h) {
                (data[point] + h) ->~T();
            }
        }
        for ( size_t h=0; h<i; ++h) {
            delete[] reinterpret_cast < int8_t* > (data[h]);
        }
        throw;
    }
}

template<typename T>
Deque<T>::Deque(int s, const T &temp):  capacity(s / mod + 2), sz(s) {
    int dr = s / mod;
    data.resize(dr + 2);
    size_t i= 0;
    int j = 0;
    int k =0;
    int p = 0;
    try {
        for (; i < data.size(); ++i) {
            data[i] = reinterpret_cast < T * > (new int8_t[mod * sizeof(T)]);
        }
        for (; j <= s / mod; ++j) {
            for (; k < mod; ++k)
                new(data[j] + k) T(temp);
        }
        for (; p < s % mod; ++p) {
            new(data[dr + 1] + p) T(temp);
        }
    }
    catch (...) {
        for (int g = 0; g < p; ++g) {
            (data[dr + 1] + g) -> ~T();
        }
        for (int g=0; g < j; ++g) {
            for (int h=0; h<mod; ++h)
                (data[g] + h)->~ T();
        }
        for (int h=0; h<k; ++h) {
            (data[j]+h)->~T();
        }
        for ( size_t g=0; g < i; ++g) {
            delete[] reinterpret_cast < int8_t* > (data[g]);
        }
        throw;
    }
    ptr.update(1, dr + 1, 0, s % mod);
}

template<typename T>
Deque<T>& Deque<T>::operator=(const Deque<T> &deq) {
    if (&deq == this) {
        return *this;
    }
    Deque < T > copy(deq);
    swap(copy);
    return *this;
}

template<typename T>
Deque<T>::~Deque() {
    if (ptr.arr_front == ptr.arr_back) {
        for (size_t i = ptr.num_front; i < ptr.num_back; ++i) {
            (data[ptr.arr_front] + i) -> ~T();
        }
    } else {
        int point = ptr.num_front;
        for (size_t i = ptr.arr_front; i < ptr.arr_back; ++i) {
            for (int j = point; j < mod; ++j) {
                (data[i] + j) -> ~T();
            }
            point = 0;
        }
        point = ptr.arr_back;
        for (size_t i = 0; i < ptr.num_back; ++i) {
            (data[point] + i) -> ~T();
        }
    }
    for (size_t i = 0; i < capacity; ++i) {
        delete[] reinterpret_cast < int8_t * > (data[i]);
    }
}

template<typename T>
void Deque<T>::push_back(const T &value) {
    if (ptr.num_back < mod - 1) {
        new(data[ptr.arr_back] + ptr.num_back) T(value);
        ++ptr.num_back;
    } else {
        if (ptr.arr_back >= capacity - 1) {
            reallocate();
        }
        new(data[ptr.arr_back] + ptr.num_back) T(value);
        ++ptr.arr_back;
        ptr.num_back = 0;
    }
    ++sz;
}

template<typename T>
void Deque<T>::push_front(const T &value) {
    if (ptr.num_front > 0) {
        --ptr.num_front;
        new(data[ptr.arr_front] + ptr.num_front) T(value);
    } else {
        if (ptr.arr_front == 0) {
            reallocate();
        }
        --ptr.arr_front;
        ptr.num_front = mod - 1;
        new(data[ptr.arr_front] + ptr.num_front) T(value);
    }
    ++sz;
}

template<typename T>
void Deque<T>::pop_back() {
    if (sz > 0) {
        if (ptr.num_back > 0) {
            --ptr.num_back;
        } else {
            --ptr.arr_back;
            ptr.num_back = mod - 1;
        }
        (data[ptr.arr_back] + ptr.num_back) -> ~T();
        --sz;
    }
}

template<typename T>
void Deque<T>::pop_front() {
    if (sz>0) {
        (data[ptr.arr_front] + ptr.num_front) -> ~T();
        if (ptr.num_front >= mod - 1) {
            ++ptr.arr_front;
            ptr.num_front = 0;
        }
        else ++ptr.num_front;
        --sz;
    }
}

template<typename T>
void Deque<T>::insert(Deque::iterator it, const T &value) {
    std::vector<T> stack;
    for (iterator i = end()-1; i>=it; --i) {
        stack.push_back(*i);
        pop_back();
    }
    push_back(value);
    while(!stack.empty()){
        push_back(stack.back());
        stack.pop_back();
    }
}

template<typename T>
void Deque<T>::erase(Deque::iterator it) {
    if (sz>0) {
        std::vector<T> stack;
        for (iterator i = end()-1; i!=it; --i){
            stack.push_back(*i);
            pop_back();
        }
        pop_back();
        while(!stack.empty()){
            push_back(stack.back());
            stack.pop_back();
        }
    }
}

template<typename T>
void Deque<T>::reallocate() {
    std::vector < T * > doto(capacity * 2);
    for (size_t i = 0; i < capacity; ++i) {
        doto[i + capacity / 2] = data[i];
    }
    size_t i=0;
    size_t j =capacity + capacity / 2;
    try {
        for (; i < capacity / 2; ++i) {
            doto[i] = reinterpret_cast < T * > (new int8_t[mod * sizeof(T)]);
        }
        for (; j < capacity*2; ++j) {
            doto[j] = reinterpret_cast < T * > (new int8_t[mod * sizeof(T)]);
        }
    }
    catch(...) {
        for (size_t h=0; h<i; ++h) {
            delete[] reinterpret_cast < int8_t* > (doto[h]);
        }
        for (size_t h=0; h<j; ++h) {
            delete[] reinterpret_cast < int8_t* > (doto[h]);
        }
        throw;
    }
    std::swap(data, doto);
    ptr.arr_front += capacity / 2;
    ptr.arr_back += capacity / 2;
    capacity *= 2;
}
