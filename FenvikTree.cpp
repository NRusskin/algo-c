/*B. Знакочередование
ограничение по времени на тест 2 секунды
ограничение по памяти на тест 64 мегабайта
ввод стандартный ввод
вывод стандартный вывод
Реализуйте структуру данных из n элементов a1,a2…an, поддерживающую следующие операции:
присвоить элементу ai значение j;
найти знакочередующуюся сумму на отрезке от l до r включительно (al−al+1+al+2−…±ar).
Входные данные
В первой строке входного файла содержится натуральное число n (1≤n≤105) — длина массива. Во второй строке записаны начальные значения элементов (неотрицательные целые числа, не превосходящие 104).
В третьей строке находится натуральное число m (1≤m≤105) — количество операций. В последующих m строках записаны операции:
операция первого типа задается тремя числами 0 i j (1≤i≤n, 1≤j≤104).
операция второго типа задается тремя числами 1 l r (1≤l≤r≤n).
Выходные данные
Для каждой операции второго типа выведите на отдельной строке соответствующую знакочередующуюся сумму.*/

#include <iostream>
#include<vector>

class FenvikTree {
   
public:    
    FenvikTree(const std::vector <long long>& p) {
        size = p.size();
        data.assign(size, 0);
        for (long long i = 0; i < size; ++i)
            inc(i, p[i]);
    }

    long long sum(long long r) {
        long long result = 0;
        for (; r >= 0; r = (r & (r + 1)) - 1)
            result += data[r];
        return result;
    }

    void inc(long long i, long long delta) {
        for (; i < size; i = (i | (i + 1)))
            data[i] += delta;
    }

    long long sum(long long l, long long r) {
        return sum(r) - sum(l - 1);
    } 
private:
    std::vector <long long> data;
    long long size;
};

int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(0);
    long long q;
    std::cin >> q;
    std::vector < long long > data(q);
    for (long long i = 0; i < q; ++i) {
        std::cin >> data[i];
        if (i % 2) data[i] *= -1;
    }
    FenvikTree fen(data);
    long long a, l, r;
    std::cin >> q;
    for (long long i = 0; i < q; ++i) {
        std::cin >> a >> l >> r;
        if (a) {
            if (l % 2) std::cout << fen.sum(l - 1, r - 1) << std::endl;
            else std::cout << -fen.sum(l - 1, r - 1) << std::endl;
        } 
        else {
            if (l % 2 == 0) {
                fen.inc(l - 1, -data[l - 1] - r);
                data[l - 1] = -r;
            } 
            else {
                fen.inc(l - 1, r - data[l - 1]);
                data[l - 1] = r;
            }
        }
    }
    return 0;
}