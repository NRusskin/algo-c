/*J. Гиперканалы
ограничение по времени на тест1 секунда
ограничение по памяти на тест256 мегабайт
вводстандартный ввод
выводстандартный вывод
В состав Галактической империи входит N планет. Между большинством из них существуют гиперканалы. 
Новый император повелел, чтобы с любой планеты можно было попасть на любую другую, пройдя только через один гиперканал. 
Каналы устроены так, что позволяют путешествовать только в одну сторону. Единственный оставшийся прокладчик гиперканалов 
расположен на базе около планеты с номером A. К сожалению, он не может путешествовать по уже существующим каналам, 
он всегда прокладывает новый. А наличие двух каналов в одном направлении между двумя планетами существенно осложняет навигацию. 
Ваша задача – найти такой маршрут для прокладчика, чтобы все необходимые каналы были построены, и не было бы построено лишних. 
В конце своего маршрута прокладчик должен оказаться на своей родной базе, с которой он начал движение.
Входные данные
В первой строке находится число N≤1000 и число A≤N. N следующих строк содержит по N чисел: в i-й строке j-е число равно 1, 
если есть канал от планеты i к планете j, иначе 0. Известно, что Галактическая империя может удовлетворить свою потребность 
в гиперканалах прокладкой не более чем 32000 новых каналов.
Выходные данные
Выведите последовательность, в которой следует прокладывать каналы. Каждая строка должна содержать два целых числа: 
номера планет, с какой и на какую следует проложить очередной гиперканал. Гиперканалы следует перечислить в порядке их прокладки.
Гарантируется, что решение существует.*/

#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>

using std::vector;
using std::cin;
using std::cout;

class CGraph {
public:
    explicit CGraph(int count): matrix(count, vector<bool>(count, false)) {}
    void AddEdge(int from, int to);
    void DeleteEdge(int from, int to);
    bool IsExistEdge(int from, int to);
    int VerticesCount() const;
private:
    vector<vector<bool>> matrix;
};
 
void CGraph::AddEdge(int from, int to) {
    matrix[from][to] = true;
}
 
void CGraph::DeleteEdge(int from, int to) {
    matrix[from][to] = false;
}

bool CGraph::IsExistEdge(int from, int to) {
    return matrix[from][to];
}

int CGraph::VerticesCount() const {
    return matrix.size();
}

void findEilerPath(CGraph& graph, vector<int>& cycle, int start) { //работает, потому что работает
    int n = graph.VerticesCount();
    std::stack<int> stack;
    stack.push(start - 1);
    while (!stack.empty()) {
        int v = stack.top();
        int i;
        for (i = 0; i < n; ++i) {
            if (graph.IsExistEdge(v, i)) { //ну потому что bfs работает ведь...
                break;
            }
        }
        if (i == n) {
            cycle.push_back(v); //ну потому что пока есть ребра, мы по ним будет ходить. А в ответ складываем в порядке обхода строго
            stack.pop();
        } 
        else {
            graph.DeleteEdge(v,i);
            stack.push(i);
        }
    }
    std::reverse(cycle.begin(), cycle.end());
}

int main() {
    int n, m;
    bool path;
    cin >> n >> m;
    CGraph graph(n);
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j) {
            cin >> path;
            if (i != j && !path) {
                graph.AddEdge(i, j);
            }
        }
    }
    vector<int> cycle;
    findEilerPath(graph, cycle, m);
    for (size_t i = 0; i < cycle.size() - 1; ++i) {
        cout << cycle[i] + 1 << ' ' << cycle[i + 1] + 1 << std::endl;
    }
}